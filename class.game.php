<?php

class Game
{
	private $id, $name, $thumbnail, $bggid, $comment, $minplayers, $maxplayers, $age, $length, $pubname, $pubid, $location, $bestplayers, $sugplayers, $altnames, $designers, $artists, $mechanics, $categories;
	function __construct( $args )
	{
		$defaults = array(
			'minplayers' => 0,
			'maxplayers' => 0,
			'age' => 0,
			'length' => 0,
			'location' => '',
			'thumbnail' => '',
			'bggid' => 0,
			'comment' => '',
			'pubname' => '',
			'pubid' => 0,
			'bestplayers' => '',
			'sugplayers' => '',
			'altnames' => '');
		$args = wp_parse_args($args, $defaults);
		extract( $args, EXTR_SKIP );
		$this->id = $id;
		$this->name = $name;
		$this->minplayers = $minplayers;
		$this->maxplayers = $maxplayers;
		$this->age = $age;
		$this->length = $length;
		$this->location = $location;
		$this->thumbnail = $thumbnail;
		$this->bggid = $bggid;
		$this->comment = $comment;
		$this->pubname = $pubname;
		$this->pubid = $pubid;
		$this->bestplayers = $bestplayers;
		$this->sugplayers = $sugplayers;
		$this->altnames = $altnames;
	}

	public static function from_id($id)
	{
		global $wpdb;
		$query = $wpdb->prepare("SELECT g.name as name, g.id as id, `thumbnail`, `bggid`, `comment`, `minplayers`, `maxplayers`, `age`, `length`, IFNULL(pub.name, '')  as publisher_name, g.publisher, `location`, `bestplayers`, `sugplayers`, `altnames` FROM `".$wpdb->prefix."games` as g LEFT JOIN `".$wpdb->prefix."games_publishers_name` as pub ON pub.id=g.publisher WHERE g.id=%d", $id);
		$row=$wpdb->get_row($query);
		if (!$row)
		{
			return false;
		}
		return new Game($row);
	}

	public function get_id()
	{
		return $this->id;
	}

	public function get_name()
	{
		return $this->name;
	}

	public function get_bggid()
	{
		return $this->bggid;
	}

	public function get_thumbnail()
	{
    		return content_url() . '/uploads/bgg/'.$this->thumbnail;
	}

	public function get_comment()
	{
		return $this->comment;
	}

	public function get_minplayers()
	{
		return $this->minplayers;
	}

	public function get_maxplayers()
	{
		return $this->maxplayers;
	}

	public function get_players()
	{
		if ($this->get_minplayers() == $this->get_maxplayers())
		{
			return $this->get_minplayers();
		}
		else
		{
			return $this->get_minplayers()."-".$this->get_maxplayers();
		}
	}

	public function get_age()
	{
		return $this->age;
	}

	public function get_length()
	{
		return $this->length;
	}

	public function get_pubname()
	{
		return $this->pubname;
	}

	public function get_pubid()
	{
		return $this->pubid;
	}

	public function get_location()
	{
		return $this->location;
	}

	public function get_bestplayers()
	{
		return $this->bestplayers;
	}

	public function get_sugplayers()
	{
		return $this->sugplayers;
	}

	public function get_altnames()
	{
		return $this->altnames;
	}

	public function get_designers()
	{
		global $wpdb;
		if (!isset($this->designers))
		{
			$this->designers = array();
    			$query = "select d.id, IFNULL(n.name, 'Inconnu') as name FROM `".$wpdb->prefix."games_designers` as d LEFT JOIN  `".$wpdb->prefix."games_people_name` as n ON d.id=n.id WHERE d.game_id='".$this->get_id()."'";
			$res = $wpdb->get_results($query);
			foreach ($res as $row)
			{
				$this->designers[$row->name] = $row->id;
			}
		}
		return $this->designers;
	}

	public function get_artists()
	{
		global $wpdb;
		if (!isset($this->artists))
		{
			$this->artists = array();
			$query = "select a.id, IFNULL(n.name, 'Inconnu') as name FROM `".$wpdb->prefix."games_artists` as a LEFT JOIN  `".$wpdb->prefix."games_people_name` as n ON a.id=n.id WHERE a.game_id='".$this->get_id()."'";
			$res = $wpdb->get_results($query);
			foreach ($res as $row)
			{
				$this->artists[$row->name] = $row->id;
			}
		}
		return $this->artists;
	}

	public function get_mechanics()
	{
		global $wpdb;
		if (!isset($this->mechanics))
		{
			$this->mechanics = array();
			$query = "select a.id, IFNULL(n.name, 'Inconnu') as name FROM `".$wpdb->prefix."games_mechanics` as a LEFT JOIN  `".$wpdb->prefix."games_mechanics_name` as n ON a.id=n.id WHERE a.game_id='".$this->get_id()."'";
			$res = $wpdb->get_results($query);
			foreach ($res as $row)
			{
				$this->mechanics[$row->name] = $row->id;
			}
		}
		return $this->mechanics;
	}

	public function get_categories()
	{
		global $wpdb;
		if (!isset($this->categories))
		{
			$this->categories = array();
			$query = "select a.id, IFNULL(n.name, 'Inconnu') as name FROM `".$wpdb->prefix."games_categories` as a LEFT JOIN  `".$wpdb->prefix."games_categories_name` as n ON a.id=n.id WHERE a.game_id='".$this->get_id()."'";
			$res = $wpdb->get_results($query);
			foreach ($res as $row)
			{
				$this->categories[$row->name] = $row->id;
			}
		}
		return $this->categories;
	}

	public function get_link()
	{
		return get_permalink(get_option('bgg_post_id')).'?game_id='.$this->id;
	}
}
?>
