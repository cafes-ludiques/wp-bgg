<?php
define ("BGG_URI", "http://www.boardgamegeek.com/xmlapi/");
define ("BGG_URI2", "http://www.boardgamegeek.com/xmlapi2/");

class BGGConnectError extends Exception {}
class BGGTempError extends Exception {}


class BGG
{
  function search_game($name)
  {
    $uri = BGG_URI . "search?search=" . urlencode($name) . "&exact=1";
    $xml = simplexml_load_string(file_get_contents($uri));
    if (!$xml)
    {
      throw new BGGConnectError("Impossible de se connecter sur BoardGameGeek ou de parser la page $uri");
    }
    $result = $xml->xpath("/boardgames/boardgame[name/text()='".$name."']/@objectid");
    if (!$result || count($result) == 0)
    {
      throw new Exception ("Impossible de trouver $name");
    }
    if (count($result) > 1)
    {
      throw new Exception ("Trop de résultat pour $name");
    }
    return $result[0][0];
  }


  function get_geeklist($id,$filter=array('id', 'objectid', 'name', 'thumbnail', 'comment', 'geeklisttitle', 'geeklistcomment'))
  {
    $uri = BGG_URI . "geeklist/$id";
    $xml = simplexml_load_string(file_get_contents($uri));
    if (!$xml)
    {
      throw new BGGConnectError("Impossible de se connecter sur BoardGameGeek ou de parser la page $uri");
    }
    $ret = array();
    foreach($xml->item as $node)
    {
      $entry = array();
      foreach ($filter as $attr)
      {
        switch($attr)
        {
        case 'id':
          $value=(string)$node['id'];
          break;
        case 'objectid':
          $value=(string)$node['objectid'];
          break;
        case 'name':
          $value=(string)$node['objectname'];
          break;
        case 'thumbnail':
          $value='http://images.boardgamegeek.com/images/pic'.$node['imageid'].'_t.jpg';
          break;
        case 'comment':
          $value=(string)$node->body;
          break;
        default:
          $value='';
          break;
        }
        $entry[$attr]=$value;
      }
      array_push($ret, $entry);
    }
    if (in_array('geeklisttitle', $filter) and count($ret) > 0)
    {
      $ret[0]['geeklisttitle']=(string)$xml->title;
    }
    if (in_array('geeklistcomment', $filter) and count($ret) > 0)
    {
      $ret[0]['geeklistcomment']=(string)$xml->description;
    }
    return $ret;
  }

  function get_collection($user,$filter=array('id', 'objectid', 'name', 'thumbnail', 'comment', 'want_to_play', 'publisher', 'wishlist', 'wishlistcomment'))
  {
    $uri = BGG_URI2 . "collection?username=$user&version=1";
    //$uri = BGG_URI2 . "collection?username=$user";
    ini_set('default_socket_timeout', 120);
    $xml = simplexml_load_string(file_get_contents($uri));
    if (!$xml)
    {
	    echo "error";
      throw new BGGConnectError("Impossible de se connecter sur BoardGameGeek ou de parser la page $uri");
    }
    if ($xml->getName() == 'message')
    {
	    echo "<p>BGG a renvoyé le message suivant : ".(string)$xml."</p>";
	    echo "<p>L'erreur est peut-être temporaire, merci de réessayer plus tard</p>";
	    throw new BGGTempError("Impossible de récupérer les informations sur BoardGameGeek, BGG a renvoyé le message suivant : <strong>".(string)$xml."</strong><br/><br/>L'erreur est peut-être temporaire, merci de réessayer plus tard.");
    }
    $ret = array();
    foreach($xml->item as $node)
    {
      $entry = array();
      foreach ($filter as $attr)
      {
        switch($attr)
	{
	case 'id':
	  $value=(string)$node['collid'];
	  break;
        case 'objectid':
          $value=(string)$node['objectid'];
          break;
        case 'name':
          $value=(string)$node->name;
          break;
        case 'thumbnail':
          $value=(string)$node->thumbnail;
          break;
        case 'comment':
          $value=(string)$node->comment;
          break;
        case 'want_to_play':
          $value=(string)$node->status['wanttoplay'];
          break;
        case 'wishlist':
          $value=(string)$node->status['wishlist'];
          break;
        case 'publisher':
          $value = 0;
          if ($node->version)
          {
	    if ($node->version->publisher['publisherid'])
	    {
              $value=(string)$node->version->publisher['publisherid'];
	    }
	    else
	    {
		    if ($node->version->item)
		    {
			    foreach ($node->version->item->link as $v_link)
			    {
				    if ($v_link['type'] == "boardgamepublisher")
				    {
					    $value = $v_link['id'];
					    break;
				    }
			    }
		    }
	    }
          }
          break;
        case 'wishlistcomment':
          $value=(string)$node->wishlistcomment;
          break;
        default:
          $value='';
          break;
        }
        $entry[$attr]=$value;
      }
      array_push($ret, $entry);
    }
    return $ret;
  }

  function get_game_extended_info($games_id, $filter=array('minplayers', 'maxplayers', 'bestplayers', 'length', 'age', 'mechanic', 'category', 'designer', 'artist', 'name'))
  {
    if (!is_array($games_id))
    {
      $games_id = array($games_id);
    }
    $uri = BGG_URI . "boardgame/" . urlencode(join(',',$games_id));
    $xml = simplexml_load_string(file_get_contents($uri));
    if (!$xml)
    {
      throw new BGGConnectError("Impossible de se connecter sur BoardGameGeek ou de parser la page $uri");
    }
    $ret=array();
    foreach($xml->boardgame as $node)
    {
      $entry = array();
      foreach ($filter as $attr)
      {
        switch($attr)
        {
        case 'minplayers':
          $value=(string)$node->minplayers;
          break;
        case 'maxplayers':
          $value=(string)$node->maxplayers;
          break;
        case 'length':
          $value=(string)$node->playingtime;
          break;
        case 'age':
          $value=(string)$node->age;
          break;
        case 'mechanic':
          $value=array();
          foreach ($node->boardgamemechanic as $mech)
          {
            array_push($value,(string) $mech['objectid']);
          }
          break;
        case 'category':
          $value=array();
          foreach ($node->boardgamecategory as $cat)
          {
            array_push($value,(string) $cat['objectid']);
          }
          break;
        case 'designer':
          $value=array();
          foreach ($node->boardgamedesigner as $designer)
          {
            array_push($value,(string) $designer['objectid']);
          }
          break;
        case 'artist':
          $value=array();
          foreach ($node->boardgameartist as $artist)
          {
            array_push($value,(string) $artist['objectid']);
          }
	  break;
	case 'bestplayers':
	  $best=array();
	  $sug=array();
	  foreach ($node->poll as $poll)
	  {
	    if ($poll['name'] != 'suggested_numplayers') continue;
	    foreach ($poll->results as $result)
	    {
	      $numvotes = 0;
	      $numvotes_notrecom = 0;
	      $numvotes_best = 0;
              foreach ($result->result as $item)
              {
                $numvotes += $item['numvotes'];
                if ($item['value'] == 'Not Recommended')
                {
                  $numvotes_notrecom = $item['numvotes'];
                }
                if ($item['value'] == 'Best')
                {
                  $numvotes_best = $item['numvotes'];
                }
              }
	      if ($numvotes_notrecom < $numvotes/2) // si moins de la moitié votent not recommended, le jeux est recommended
	      {
		      if (is_numeric((string)$result['numplayers'])) // on ignore les "n+"
		      {
	                array_push($sug, (string)$result['numplayers']);
	              }
	      }
	      if (($numvotes_best!=0) && ($numvotes_best >= $numvotes/2))
	      {
		      if (is_numeric((string)$result['numplayers'])) // on ignore les "n+"
		      {
	                array_push($best, (string)$result['numplayers']);
	              }
	      }

	    }
	  }
	  $value = array($sug, $best);
	  break;
        case 'name':
          $value=array();
          foreach ($node->name as $name)
          {
            array_push($value,(string) $name);
          }
          break;
        default:
          $value='';
          break;
        }
        $entry[$attr]=$value;
      }
      $ret[(string)$node['objectid']]=$entry;
    }
    return $ret;
  }

  function get_publisher_name($pub_id)
  {
    $uri = BGG_URI . "boardgamepublisher/" . urlencode($pub_id);
    $xml = simplexml_load_string(file_get_contents($uri));
    if (!$xml)
    {
      throw new BGGConnectError("Impossible de se connecter sur BoardGameGeek ou de parser la page $uri");
    }
    foreach($xml->company as $node)
    {
      return (string)$node->name;
    }
    return "Inconnu";
  }

  function get_person_name($person_id)
  {
    $uri = BGG_URI . "boardgameperson/" . urlencode($person_id);
    $xml = simplexml_load_string(file_get_contents($uri));
    if (!$xml)
    {
      throw new BGGConnectError("Impossible de se connecter sur BoardGameGeek ou de parser la page $uri");
    }
    foreach($xml->person as $node)
    {
      return (string)$node->name;
    }
    return "Inconnu";
  }
}
