<?php 
/*
* * * * Plugin Name: BGG
* * * * Plugin URI: https://gitlab.com/cafes-ludiques/wp-bgg
* * * * Description: This plugin will import list of games from Boardgamegeek
* * * * Version: 1.2
* * * * Author: Gregoire Menuel
* * * * Author URI: http://gregoire.menuel.eu
* * * * License: AGPLv3
* * * * */

function bgg_install()
{
	global $wpdb;
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	$sql = "CREATE TABLE `".$wpdb->prefix."games` (`id` int(10) AUTO_INCREMENT PRIMARY KEY, `name` varchar(256), `thumbnail` varchar(200), `bggid` int(10), `comment` text, `minplayers` int(3), `maxplayers` int(3), `age` int(3), `length` int(4), `publisher` int(10), `location` enum('party','ludo','2','xs','s','m','l','coop','famille','adresse','lettre','lettres','traduire','vitrine','sac','wargame'), `new` boolean, `bestplayers` set('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'), sugplayers set( '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21' ), altnames TEXT, last_update DATETIME, INDEX(`list_id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_by_list` (game_id int(10), list_id varchar(32), type enum('user','list'), primary key (game_id,list_id))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_lists` (`id` int(10) AUTO_INCREMENT PRIMARY KEY, `name` varchar(256), `type` ENUM('geeklist','collection','sublist'), `bggid` varchar(256), `comment` text, `title` varchar(256), `parent` INT(11), `last_collid` INT(11), UNIQUE(`name`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_mechanics` (`id` int(10), `game_id` int(10), PRIMARY KEY (`id`, `game_id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_mechanics_name` (`id` int(10), `name` varchar(255), PRIMARY KEY (`id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_categories` (`id` int(10), `game_id` int(10), PRIMARY KEY (`id`, `game_id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_categories_name` (`id` int(10), `name` varchar(255), PRIMARY KEY (`id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_designers` (`id` int(10), `game_id` int(10), PRIMARY KEY (`id`, `game_id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_artists` (`id` int(10), `game_id` int(10), PRIMARY KEY (`id`, `game_id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_people_name` (`id` int(10), `name` varchar(255), PRIMARY KEY (`id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."games_publishers_name` (`id` int(10), `name` varchar(255), PRIMARY KEY (`id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."parties` (`id` int(10) AUTO_INCREMENT PRIMARY KEY, `game_id` varchar(255), `user` varchar(255), `date` DATETIME, `max` int(10), officiel int(1) DEFAULT 0, `admin_key` varchar(13) UNIQUE, comment text, `email` varchar(255), `creation_date` DATETIME, INDEX (`game_id`))";
	dbDelta($sql);
	$sql = "CREATE TABLE `".$wpdb->prefix."parties_replies` (`id` int(10) AUTO_INCREMENT PRIMARY KEY, `partie_id` int(10), `user` varchar(255), `email` varchar(255), date DATETIME, comment text, `admin_key` varchar(13) UNIQUE , INDEX (`partie_id`))";
	dbDelta($sql);
}


add_action('admin_menu', 'bgg_menu');

function bgg_menu() {
	add_submenu_page('tools.php', 'BGG', 'BGG', 'manage_options', 'bgg-options-id', 'bgg_plugin_options');
}

function bgg_plugin_options() {
	global $wpdb;
	include_once('class.games.php');
	echo "<h2>Configuration du plugin BGG</h2>";
		if (!current_user_can('manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}

		$plugin_url = menu_page_url('bgg-options-id', false);
		$default_list = get_option('bgg_default_list');
		$post_id = get_option('bgg_post_id');
		$new_partie_post = get_option('bgg_new_partie_post');
		$join_partie_post = get_option('bgg_join_partie_post');
		$mail_from = get_option('bgg_mail_from');
		$bronze = get_option('bgg_bronze');
		$silver = get_option('bgg_silver');
		$gold = get_option('bgg_gold');
		if (!$default_list) { $default_list = 1;}

		$list_id = isset($_REQUEST['list_id'])?$_REQUEST['list_id']:'';
		$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
		$type_list = isset($_POST['type'])?$_POST['type']:'';
		$list_name = isset($_POST['name'])?$_POST['name']:'';
		$bggid=isset($_POST['bggid'])?$_POST['bggid']:'';
		if ($action == 'add')
		{
			if ( $list_name == '' || $type_list == '' || ($bgg_id='' && $type_list != 'sublist'))
			{
				echo "<p>Paramètres d'ajout insuffisant</p>";
			}
			else
			{
				$now=time();
				$parent='null';
				if ($type_list == 'sublist')
				{
					$parent = $default_list;
				}
				$query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_lists` VALUES (null, %s, %s, %s, '', '', %s, 0)", $list_name, $type_list, $bggid, $parent);
				if (!$wpdb->query($query))
				{
					echo "Impossible de créer la liste, le serveur a renvoyé : ";
					$wpdb->print_error();
				}
				else
				{
					$list_id=$wpdb->insert_id;
					if ($type_list == 'sublist')
					{
						echo "<p>Liste créée. Cliquez <a href='$plugin_url&amp;list_id=$list_id&amp;action=edit'>ici</a> pour ajouter des jeux.</p>";
					}
					else
					{
						try {
							Games::import_list_items($list_id, $type_list, $bggid);
						}
						catch (BGGTempError $e) {
							header("Refresh:60");
							print $e->getMessage();
							print "<p>Un nouvel essai sera fait dans une minute</p>";
							throw $e;
						}
						echo "<p>Import de la liste terminé en ".(time()-$now)." secondes.</p>";
					}
				}
			}
		}
		elseif($action == "edit")
		{
			if ($list_id != '')
			{
				echo Games::display_sublist_form($list_id, $plugin_url, 'list');
			}
		}
		elseif($action == "edit2")
		{
			if ($list_id != '')
			{
				echo Games::submit_sublist_form($list_id, $_POST['games'], "$plugin_url&amp;list_id=$list_id&amp;action=edit", 'list');
			}
		}
		elseif ($action == "update")
		{
			$now = time();
			$query=$wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_by_list` WHERE list_id=%s", $list_id."_temp");
			$wpdb->query($query);
			$query = $wpdb->prepare("SELECT type, bggid FROM `".$wpdb->prefix."games_lists` WHERE id=%s", $list_id);
			$row = $wpdb->get_row($query);
			if ($row)
			{
				try {
					$list = Games::import_list_items($list_id."_temp", $row->type, $row->bggid);
				}
				catch (BGGTempError $e) {
					header("Refresh:60");
					print $e->getMessage();
					print "<p>Un nouvel essai sera fait dans une minute</p>";
					throw $e;
				}
				$query = $wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_by_list` WHERE list_id=%s", $list_id);
				$wpdb->query($query);
				$query = $wpdb->prepare("UPDATE `".$wpdb->prefix."games_by_list` SET list_id=%s WHERE list_id=%s", $list_id, $list_id."_temp");
				$wpdb->query($query);
				echo "<p>Import de la liste des jeux en ".(time()-$now)." secondes.</p>";
				echo "<p><strong style='font-size: 200%'><a href='$plugin_url&amp;list_id=$list_id&amp;action=update_infos'>Cliquez ici pour mettre à jour les infos des jeux</a></strong></p>";
				echo "<p>Rechargement automatique après 30 secondes</p>";
				echo "<script type=\"text/javascript\">setTimeout(function(){ window.location.href = '$plugin_url&list_id=$list_id&action=update_infos'; }, 30000); </script>";
			}
			else
			{
				echo "<p>Liste $list_id introuvable</p>";
			}

		}
		elseif ($action == "update_infos")
		{
			$now = time();
			$query = $wpdb->prepare("SELECT type, bggid FROM `".$wpdb->prefix."games_lists` WHERE id=%s", $list_id);
			$row = $wpdb->get_row($query);
			if ($row)
			{
				$list = Games::update_games_info($list_id);
				echo "<p>Import de la liste terminé en ".(time()-$now)." secondes.</p>";
			}
			else
			{
				echo "<p>Liste $list_id introuvable</p>";
			}

		}
		if ($list_id != '')
		{
			if ($action != "edit")
			{
				$query = $wpdb->prepare("SELECT * FROM `".$wpdb->prefix."games` as g, `".$wpdb->prefix."games_by_list` as l WHERE g.id=l.game_id AND l.list_id=%s ORDER BY NAME ASC", $list_id);
				$res = $wpdb->get_results($query);
				echo '<table>'."\n";
				foreach($res as $row)
				{
					echo '<tr><td><a href="http://boardgamegeek.com/boardgame/'.$row->bggid.'">'.$row->name.'</a></td><td>'.$row->comment.'</td></tr>';
				}
				echo '</table>';
				echo '<a href="'.$plugin_url.'">Retour</a>';
			}
		}
		else
		{
			if (isset($_POST['default_list']) || isset($_POST['post_id']) || isset($_POST['new_partie_post']) || isset($_POST['join_partie_post']))
			{
				$default_list = isset($_POST['default_list'])?$_POST['default_list']:$default_list;
				$post_id = isset($_POST['post_id'])?$_POST['post_id']:$post_id;
				$new_partie_post = isset($_POST['new_partie_post'])?$_POST['new_partie_post']:$new_partie_post;
				$join_partie_post = isset($_POST['join_partie_post'])?$_POST['join_partie_post']:$join_partie_post;
				$bronze = (isset($_POST['bronze']) && is_numeric($_POST['bronze']))?$_POST['bronze']:1;
				$silver = (isset($_POST['silver']) && is_numeric($_POST['silver']))?$_POST['silver']:5;
				$gold = (isset($_POST['gold']) && is_numeric($_POST['gold']))?$_POST['gold']:10;
				$mail_from = isset($_POST['mail_from'])?$_POST['mail_from']:"null@example.com";
				update_option('bgg_default_list', $default_list);
				update_option('bgg_post_id', $post_id);
				update_option('bgg_new_partie_post', $new_partie_post);
				update_option('bgg_join_partie_post', $join_partie_post);
				update_option('bgg_bronze', $bronze);
				update_option('bgg_silver', $silver);
				update_option('bgg_gold', $gold);
				update_option('bgg_mail_from', $mail_from);
			}
			$query = "SELECT list.id as list_id, list.name as name, count(l.game_id) as count, list.type as type FROM `".$wpdb->prefix."games_lists` as list LEFT JOIN `".$wpdb->prefix."games_by_list` as l ON l.list_id=list.id GROUP BY list.id ORDER BY list.name";
			$res = $wpdb->get_results($query);
			echo '<ul>'."\n";
			foreach ($res as $row)
			{
				echo '<li><a href="'.get_permalink($post_id).'?list_id='.$row->list_id.'">'.$row->name.'</a> ('.$row->count.') ';
				if ($row->type == 'sublist')
				{
					echo '<a href="'.$plugin_url.'&amp;list_id='.$row->list_id.'&amp;action=edit">Editer</a> ';
				}
				else
				{
					echo '<a href="'.$plugin_url.'&amp;action=update&amp;list_id='.$row->list_id.'">Update</a>';
				}  
				echo '</li>';
			}
			echo '</ul>';
			$type_list=array('Sous-liste'=>'sublist','Collection'=>'collection', 'Geeklist'=>'geeklist');
			echo '<h2>Créer une liste</h2><form action="" method="post">Nom : <input type="text" name="name" id="name" size="10" maxlength="50"/><input type="hidden" name="action" value="add"/> <br/>Type : <select name="type" id="type" >';
			foreach ($type_list as $title => $name)
			{
				echo '<option value="'.$name.'">'.$title.'</option>';
			}
			echo '</select><br/>Identifiant de geeklist ou de l\'utilisateur : <input type="text" name="bggid" id="bggid" size="10" maxlength="50"/><br/><input type="submit" value="Valider" /></form></p>';

			echo '<h2>Configuration</h2>';
			echo '<form action="" method="post">';
			echo 'Liste par défaut : <select name="default_list">';
			$query = "SELECT id,name from `".$wpdb->prefix."games_lists` ORDER BY name";
			$res = $wpdb->get_results($query);
			foreach ($res as $row)
			{
				echo '<option value="'.$row->id.'"';
				if ($row->id == $default_list) echo ' selected="selected"';
				echo '>'.$row->name.'</option>';
			}
			echo '</select><br/>';
			echo 'Page contenant la ludothèque : '. wp_dropdown_pages(array('selected'=>$post_id, 'echo'=>0, 'name'=>'post_id')).'<br/>';
			echo 'Page pour créer des parties : '. wp_dropdown_pages(array('selected'=>$new_partie_post, 'echo'=>0, 'name'=>'new_partie_post')) .'<br/>';
			echo 'Page pour s\'inscrire à une partie : '. wp_dropdown_pages(array('selected'=>$join_partie_post, 'echo'=>0, 'name'=>'join_partie_post,')).'<br/>';
			echo 'Adresse e-mail expéditeur des mails pour les parties : <input type="text" name="mail_from" value="'.htmlentities($mail_from, ENT_COMPAT, 'UTF-8').'"/><br/>';
			echo 'Seuil des médailles : <br/>';
			echo 'Bronze : <input type="text" name="bronze" value="'.htmlentities($bronze, ENT_COMPAT, 'UTF-8').'"/><br/>';
			echo 'Argent : <input type="text" name="silver" value="'.htmlentities($silver, ENT_COMPAT, 'UTF-8').'"/><br/>';
			echo 'Or : <input type="text" name="gold" value="'.htmlentities($gold, ENT_COMPAT, 'UTF-8').'"/><br/>';

			echo '<input type="submit" value="Valider"/></form>';
		}

}



function bgg_display($atts, $content=null)
{
	$output = "";
	include_once('class.games.php');
	$default_list = get_option('bgg_default_list');
	extract( shortcode_atts( array(
		'list' => $default_list
	), $atts ) );

	$game_id = isset($_GET['game_id'])?$_GET['game_id']:'';
	$new = isset($_GET['new'])?$_GET['new']:'';
	$list_id = isset($_GET['list_id'])?$_GET['list_id']:$list;
	$type_gl = isset($_GET['type'])?$_GET['type']:'list';
	$mechanic = isset($_GET['mechanic'])?$_GET['mechanic']:'';
	$category = isset($_GET['category'])?$_GET['category']:'';
	$emplacement = isset($_GET['emplacement'])?$_GET['emplacement']:'';
	$designer = isset($_GET['designer'])?$_GET['designer']:'';
	$artist = isset($_GET['artist'])?$_GET['artist']:'';
	$search = isset($_GET['search'])?true:false;
	if (is_array($list_id))
	{
		$list_id_uri = "list_id[]=".implode("&amp;list_id[]=", array_map("urlencode",$list_id));
	}
	else
	{
		$list_id_uri = "list_id=".urlencode($list_id);
	}
	$type_gl_esc = urlencode($type_gl);
	if ($game_id != '')
	{
		$output .= Games::display_game($game_id);
	}
	else
	{
		if ($search)
		{
			$output .= "<h2 class='entry-title'>Recherche</h2>";
			$search_param = $_REQUEST;
			$s_param=Games::get_search_parameters($search_param);
			if (count($s_param) == 0) // pas de param, donc on veut faire une nouvelles recherche
			{
				$output .= "<p><br/><a href='".get_permalink($post_id)."?".$list_id_uri."&amp;type=".$type_gl_esc."'>Liste complète des jeux</a></p>";
				$output .= "<form action='".get_permalink($post_id)."?".$list_id_uri."&amp;type=".$type_gl_esc."&amp;search' method='post'>";
				$output .= "<strong>Nombre de joueurs</strong> <input type='text' name='players' size='3'/> <select name='op_players'><option value='default'>Possible</option><option value='sug'>Conseillé</option><option value='best'>Idéal</option></select><br/>\n";
				$output .= "<strong>Age minimum</strong> <input type='text' name='age' size='3'/><br/>\n";
				$output .= "<strong>Durée maximum</strong> (en minutes) <input type='text' name='length' size='3'/><br/>\n";
				echo "<p>Vous pouvez sélectionner ou désélectionner des choix en appuyant sur CTRL et en cliquant sur le paramètre.</p>";
				$type_list = array ("emplacement" => "Emplacement", "mechanics" => "Mécanismes", "categories" => "Catégories", "designers" => "Créateurs", "artists" => "Illustrateurs");
				foreach ($type_list as $type => $trad)
				{
					$output .= "<strong>$trad</strong><br/> ";
					if ($type != 'emplacement')
					{
						$output .= "<input type='radio' id='op_or_$type' name='op_$type' checked='checked' value='OR'/> <label for='op_or_$type'>Au moins un parmis ceux sélectionnés</label> <br/><input type='radio' id='op_and_$type' name='op_$type' value='AND' /> <label for='op_and_$type'>Tous ceux sélectionnés</label> <br/>\n";
					}
					$output .= "<select name='".$type."[]' multiple='multiple' size='5'>";
					foreach (Games::get_list_choice($type, $list_id, $type_gl) as $id=>$value)
					{
						$output .= "<option value='$id'>$value</option>\n";
					}
					$output .= "</select><br/><br/>\n";
				}
				$output .= "<input type='submit' value='Rechercher'/></form>";
			}
			else
			{
				$output .= "<p>Critères de recherche :<ul><li>".implode("</li><li>", $s_param)."</li></ul></p>";
				$output .= "<p><br/><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."&amp;search'>Formulaire de recherche</a><br/>";
				$output .= "<a href='".Games::get_search_uri($search_param, $list_id, $type_gl)."'>Lien vers cette page</a><br/>";
				$output .= "<a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."'>Liste complète des jeux</a></p>";
				$output .= Games::list_games_by_search($search_param, $list_id, $type_gl);
			}
			$output .= "<p><br/><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."'>Liste complète des jeux</a>";
			if (count($s_param) != 0) // on n'affiche le lien suivant que si il n'y a pas de param de recherche
				$output .= "<br/><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."&amp;search'>Formulaire de recherche</a>";
			$output .= "</p>";
		}
		else
		{
			if ($new)
			{
				$output .= "<h2 class='entry-title'>Liste des nouveautés</h2>";
			}
			else
			{
				if ($list_id == $default_list && $content) # liste par defaut et balise content setté, on affiche $content au début de la page
				{

					$output .= do_shortcode($content);
				}
				$output .= "<h2 class='entry-title'>Liste des jeux</h2>";
			}
			$output .= "<p><br/><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."&amp;search'>Formulaire de recherche</a></p>";
			if ($new)
			{
				$output .= "<p><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."'>Voir tous les jeux</a></p>";
				$output .= Games::list_new_games($list_id);
				$output .= "<p><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."'>Voir tous les jeux</a></p>";
			}
			else
			{
				$output .= Games::list_games($list_id, 0, $type_gl);
			}
			$output .= "<p><a href='".get_permalink()."?".$list_id_uri."&amp;type=".$type_gl_esc."&amp;search'>Formulaire de recherche</a></p>";
		}
	}
	return $output;
}

function bgg_images_display($atts)
{
	include_once('class.games.php');
	$default_list = get_option('bgg_default_list');
	extract( shortcode_atts( array(
		'list' => $default_list,
		'num' => 4,
		'new' => false
	), $atts ) );
	return Games::games_images ($list, $num, $new);
}

function bgg_game_display($atts)
{
	include_once('class.game.php');
	include_once('class.games.php');
	extract( shortcode_atts( array(
		'id' => 0
	), $atts ) );
	$game = Game::from_id($id);
	if (!$game)
		return "Jeu $id introuvable";
	$output = "<a class='bgg_game' href='".$game->get_link()."'><div><img src='".$game->get_thumbnail()."' alt='Boite du jeu' title='".htmlentities($game->get_name(), ENT_QUOTES|ENT_SUBSTITUTE|ENT_HTML5, 'UTF-8')."'/><p><strong>".$game->get_name()."</strong><br/><br/>".$game->get_players()." joueurs<br/>".$game->get_length()." minutes</p></div></a>"; 
	return $output;
}

function bgg_title($title)
{
	if (get_the_ID() !=  get_option('bgg_post_id'))
	{
		return $title;
	}
	include_once('class.games.php');
	if (isset($_REQUEST['game_id']))
	{
		$text = Games::get_game_name($_REQUEST['game_id']);
	}
	if ($text)
	{
		return $title.$text." | ";
	}
	return $title;
}

function bgg_explications_display($atts)
{
	include_once('class.games.php');
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
	$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
	$coeur = isset($_REQUEST['coeur'])?$_REQUEST['coeur']:'';
	$best = isset($_REQUEST['best'])?$_REQUEST['best']:'';
	extract( shortcode_atts( array(), $atts ) );
	$output = "";
	switch($action)
	{
	case 'edit':
		$user_rs = get_user_by('login', $id);
		$user = trim($user_rs->display_name);
		$output .= "<p>Voici la liste des jeux pouvant être expliqués par <strong>$user</strong>.</p>";
		$output .= Games::display_sublist_form($id, get_permalink()."?action=edit2&amp;id=$id", 'user');
		break;
	case 'edit2':
		$output .= Games::submit_sublist_form($id, $_POST['games'], get_permalink()."?action=edit&amp;id=$id", 'user');
		Games::submit_sublist_form($id, $_POST['coeur'], get_permalink()."?action=edit&amp;id=$id", 'usercoeur');
		Games::submit_sublist_form($id, array($best), get_permalink()."?action=edit&amp;id=$id", 'userbest');
		break;
	default:
		$blogusers = get_users('');
		$users_list = array();
		foreach ($blogusers as $user)
		{
			$user_info = get_userdata($user->ID);
			$users_list[trim($user_info->display_name)] = $user->user_login;
		}
		setlocale(LC_COLLATE, "fr_FR.UTF8", "fr_FR.ISO8859-1", "fr_FR", "fr");
		uksort($users_list, 'compare_string');
		$output .= "<ul>";
		foreach ($users_list as $name => $id)
		{
			$output .= "<li><a href='".get_permalink()."?action=edit&amp;id=$id'>$name</a></li>\n";
		}
		$output .= "</ul>";
		break;
	}
	return $output;
}

function bgg_user_update_display($atts)
{
	extract( shortcode_atts( array(), $atts ) );
	$output = "";
	$current_user = wp_get_current_user();
	if ( 0 == $current_user->ID ) {
		// Not logged in.
		return "";
	}
	// Logged in.
	include_once("class.games.php");

	$update_date = Games::get_last_user_update($current_user->user_login);
	if (!$update_date)
	{
		$output .= "Tu n'as pas encore inscrit les jeux que tu sais expliquer. Clique ici pour le faire maintenant.";
	}
	else
	{
		$date = new DateTime($update_date);
		$diff = $date->diff(new DateTime());
		$jour = $diff->format("%a");
		if ($jour > 30)
		{
			$output .= "Tu n'as pas mis à jour les jeux que tu sais expliquer depuis $jour jours. Clique ici pour le faire maintenant.";
		}
	}
	if (!empty($output))
		return "<div class=\"info_bene\"><a href=\"".site_url()."/explications?action=edit&amp;id=".($current_user->user_login)."\">".$output.".</a></div>";
	return;
}

function coeur_rss()
{
	include_once("class.games.php");
	$title = htmlspecialchars("Jeux des bénévoles");
	$items = "";
	if (function_exists('trombi_get_url'))
	{
		$last_ts = 0;
	  	$list = Games::get_last_coeur();
		foreach ($list as $date => $line)
		{
			//  $line = htmlspecialchars(html_entity_decode($line, ENT_COMPAT, dc_encoding),ENT_NOQUOTES);
			if ($date > $last_ts)
				$last_ts = $date;
	
			$items .= '<item>'."\n".
				'  <title>'.htmlspecialchars('Nouveau coup de coeur/jeu préféré de '.$line['name'])."</title>\n".
				'  <link>'.trombi_get_url().'?id='.$line['username']."</link>\n".
				'  <pubDate>'.date("D, d M Y H:i:s O", $date)."</pubDate>\n".
				'  <guid>'.trombi_get_url().'?date='.$date."</guid>\n".
				'  <description>'.htmlspecialchars('Jeu préféré : '.$row['best'])."</description>\n".
				'</item>'."\n";
		}
	}
header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;


echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        <?php do_action('rss2_ns'); ?>
>

<channel>
        <title><?php echo $title;?></title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss("description") ?></description>
        <lastBuildDate><?php echo date("D, d M Y H:i:s O", $last_ts); ?></lastBuildDate>
        <language><?php echo get_option('rss_language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
        <?php do_action('rss2_head'); ?>
	<?php echo $items;?>
	</channel>
</rss>
<?php
}


function bgg_new_partie_display($atts)
{
	extract( shortcode_atts( array(), $atts ) );
	include_once('class.games.php');
	include_once('class.parties.php');
	$admin_key = isset($_REQUEST['admin_key'])?$_REQUEST['admin_key']:'';
	$game_id = isset($_REQUEST['game_id'])?$_REQUEST['game_id']:'';
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
	if ($action == "admin" && current_user_can('manage_options'))
	{
		$parties = Partie::get_parties('all');
		if (count($parties) == 0)
		{
			return "<p>Aucune partie.</p>";
		}
		$output = "Administration des parties organisées :<br/><ul>";
		foreach ($parties as $partie)
		{
			$output .= "<li><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."\">Le ".$partie->get_human_date()." à ".$partie->get_time()." par ".$partie->get_name_esc()." : <strong>".$partie->get_game_name()."</strong> (".$partie->get_count().($partie->get_max()>0?"/".$partie->get_max():'').")</a></li>";
		}
		$output .= "</ul>";
		return $output;
	}
	if ($admin_key) # affichage du panel d'admin de la partie
	{
		$partie = Partie::get_partie_by_admin($admin_key);
		if (!$partie)
		{
			return "<p><strong>Clé d'administration non valide.</strong></p>";
		}
		if ($action == "delete_partie" || $action == "delete_partie2") // Suppression de partie
		{
			if ($action == "delete_partie")
			{
				$output = "<div id=\"plugin-respond\"><p class=\"comment-notes\">Vous êtes sur le point de <strong>supprimer la partie</strong> de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().". Les participants seront notifiés par mail, vous pouvez ajouter un commentaire à la notification en remplissant le champs ci-dessus. Cliquez sur valider pour confirmer la suppression.</p><form method=\"post\" action=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."\">
					<input type=\"hidden\" name=\"action\" value=\"delete_partie2\"/>
					<p><label for=\"comment\">Commentaire</label><textarea id=\"comment\" cols=\"45\" rows=\"8\" name=\"comment\"></textarea></p>
					<p class=\"form-submit\"><input type=\"submit\" value=\"Valider\"/></p></form></div>";
				$output .= "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."\">Retour</a>";
			}
			elseif($action == "delete_partie2")
			{
				$comment = isset($_REQUEST['comment'])?$_REQUEST['comment']:'';
				foreach ($partie->get_replies() as $reply)
				{
					
					$headers = array("From: ".mb_encode_mimeheader(html_entity_decode(get_bloginfo('name'), ENT_COMPAT|ENT_QUOTES|ENT_HTML401))." <".get_option('bgg_mail_from').">");
					wp_mail($reply->get_email(), "La partie de ".$partie->get_game_name()." a été annulée", "L'organisateur de la partie de ".$partie->get_game_name()." du ".$partie->get_human_date()." à ".$partie->get_time()." a annulé la partie.\n\n".(($comment!='')?'Commentaire de l\'organisateur : '.$comment:''), $headers);
				}
				if ($partie->delete())
				{
					$output .= "<p>La partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time()." à été supprimée. Un mail a été envoyé aux participants.</p>";
				}
				else
				{

					$output .= "<p>Impossible de supprimer la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().".</p>";
				}
				$output .= "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."\">Retour</a>";
			}
		}
		elseif ($action == "delete" || $action == "delete2")
		{
			$reply = isset($_REQUEST['reply'])?$_REQUEST['reply']:'';
			$reply = Partie_Reply::get_reply_by_id($reply);
			if (!$reply || ($reply->get_partie_id() != $partie->get_id()))
			{
				return "<p><strong>Identifiant de réponse non valide</strong><br/><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."\">Retour</a><p>";
			}
			if ($action == "delete")
			{
				$output = "<div id=\"plugin-respond\"><p class=\"comment-notes\">Vous êtes sur le point de supprimer l'inscription de ".$reply->get_name_esc()." à la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().". ".$reply->get_name_esc()." sera notifié par mail, vous pouvez ajouter un commentaire à la notification en remplissant le champs ci-dessus. Cliquez sur valider pour confirmer la suppression.</p><form method=\"post\" action=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."&amp;reply=".$reply->get_id()."\">
					<input type=\"hidden\" name=\"action\" value=\"delete2\"/>
					<p><label for=\"comment\">Commentaire</label><textarea id=\"comment\" cols=\"45\" rows=\"8\" name=\"comment\"></textarea></p>
					<p class=\"form-submit\"><input type=\"submit\" value=\"Valider\"/></p></form></div>";
			}
			elseif($action == "delete2")
			{
				$comment = isset($_REQUEST['comment'])?$_REQUEST['comment']:'';
				if ($reply->delete())
				{
					$output .= "<p>L'inscription de ".$reply->get_name_esc()." à la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time()." à été supprimée. Un mail lui a été envoyé.</p>";
					$headers = array("From: ".mb_encode_mimeheader(html_entity_decode(get_bloginfo('name'), ENT_COMPAT|ENT_QUOTES|ENT_HTML401))." <".get_option('bgg_mail_from').">");
					wp_mail($reply->get_email(), "Votre inscription à la partie de ".$partie->get_game_name()." a été supprimée", "L'organisateur de la partie de ".$partie->get_game_name()." du ".$partie->get_human_date()." à ".$partie->get_time()." a supprimée votre inscription.\n\n".(($comment!='')?'Commentaire de l\'organisateur : '.$comment:''), $headers);
				}
				else
				{

					$output .= "<p>Impossible de supprimer l'inscription de ".$reply->get_name_esc()." à la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().".</p>";
				}
			}
			$output .= "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."\">Retour</a>";
		}
		else
		{
			$output = "<p>Partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().".</p>";
			$output .= "<table><tr><th>Nom</th><th>E-mail</th><th>Date</th><th>Commentaire</th><th></th></tr>";
			foreach ($partie->get_replies() as $reply)
			{
				$output .= "<tr><td>".$reply->get_name_esc()."</td><td>".$reply->email."</td><td>".$reply->date."</td><td>".$reply->get_comment_esc()."</td><td><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."&amp;reply=".$reply->get_id()."&amp;action=delete\">Supprimer</a></td></tr>";
			}
			$output .= "</table>";
			$output .= "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()."&amp;action=delete_partie\">Supprimer la partie</a></p>";
		}
		return $output ;
	}
	if ($game_id && !is_numeric($game_id))
	{
		return "<p>Aucun jeu spécifié.</p>";
	}
	if ($game_id) # on fait une partie pour un jeu donné
	{
		$game_name = Games::get_game_name($game_id);
		if (!$game_id)
		{
			return "<p>Jeu introuvable.</p>";
		}
	}
	else
	{
		$game_name = "";
		$output .= "<p><strong>Pour organiser une partie d'un jeu spécifique rendez-vous sur la page du jeu en question.</strong></p>";
	}
	$ok = 0;
	if ($action == "add")
	{
		$game_name = stripslashes(isset($_REQUEST['game_name'])?$_REQUEST['game_name']:'');
		$name_user = stripslashes(isset($_REQUEST['name_user'])?$_REQUEST['name_user']:'');
		$email = stripslashes(isset($_REQUEST['email'])?$_REQUEST['email']:'');
		$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';
		$time = isset($_REQUEST['time'])?$_REQUEST['time']:'';
		$max = isset($_REQUEST['max'])?$_REQUEST['max']:'';
		$officiel = isset($_REQUEST['officiel'])?$_REQUEST['officiel']:0;
		if ($officiel != 0 && !current_user_can('create_parties'))  {
			$officiel = 0;
		}
		$comment = stripslashes(isset($_REQUEST['comment'])?$_REQUEST['comment']:'');
		$partie = new Partie(0, $game_id, $game_name, $name_user, $date, $time, $max, $comment, $email, $officiel);
		$game_name = $partie->get_game_name();
		$ok = 1;
		if (!$game_id && empty($game_name))
		{
			$output .= "<p><strong>Le nom du jeu est obligatoire.</strong></p>";
			$ok = 0;
		}
		if (empty($name_user))
		{
			$output .= "<p><strong>Le champ nom est obligatoire.</strong></p>";
			$ok = 0;
		}
		if (!preg_match("/^\d\d\d\d-\d\d\-\d\d\$/", ($date = preg_replace("/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/", "\\3-\\2-\\1", $date))))
		{
			$output .= "<p><strong>Format de date incorrecte.</strong></p>";
			$ok = 0;
		}
		elseif ($date < date("Y-m-d"))
		{
			$output .= "<p><strong>Cette date est déjà passée.</strong></p>";
			$ok = 0;
		}
		if (!preg_match("/^\d\d:\d\d$/", $time))
		{
			$output .= "<p><strong>Format d'heure incorrecte.</strong></p>";
			$ok = 0;
		}
		elseif ($time > "24:00")
		{
			$output .= "<p><strong>Format d'heure incorrecte.</strong></p>";
			$ok = 0;
		}
		if ($max != '' && !is_numeric($max))
		{
			$output .= "<p><strong>Le nombre maximum doit être un nombre.</strong></p>";
			$ok = 0;
		}
		if (!bgg_validate_email($email))
		{
			$output .= "<p><strong>$email n'est pas un email valide.</strong></p>";
			$ok = 0;
		}
		if ($ok == 1)
		{
			$partie->set_date($date);
			if ($partie->save())
			{
				$output .= "<p>Partie programmée. <a href=\"".get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id()."\">Rajouter des participants</a></p>";
				if (!$officiel)
					$partie->add_reply($name_user, $email, '');
				$headers = array("From: ".mb_encode_mimeheader(html_entity_decode(get_bloginfo('name'), ENT_COMPAT|ENT_QUOTES|ENT_HTML401))." <".get_option('bgg_mail_from').">");
				wp_mail($email, "Nouvelle partie de ".$game_name, "La création de votre partie de $game_name a été prise en compte. \n\nL'administration se fait avec le lien suivant "."".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key()." . \n\nPour rajouter des participants rendez-vous sur le lien ".get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id(), $headers);
				$output .= "<p>Vous avez du recevoir un e-mail contenant le lien d'administration.</p>";
			}
			else
			{
				$output .= "<p>Problème lors de l'ajout de la partie.</p>";
			}

		}
	}
	if ($ok == 0)
	{
		$output .= "<div id=\"plugin-respond\"><p class=\"comment-notes\">Organiser une partie ".(!empty($game_name)?"pour <strong>$game_name</strong>.</p>":"");
		$output .= get_partie_form($game_id, $partie);
		$output .= "</div>";
	}

	return $output;
}

function get_partie_form($game_id, $partie=null)
{
	return '<form action="" method="post">
		'.(!$game_id?'<p><label for="game_name">Titre de la partie</label><span class="required">*</span><input type="text" id="game_name" name="game_name" value="'.(isset($partie)?htmlentities($partie->get_game_name(), ENT_COMPAT, 'UTF-8'):'').'" size="30" aria-required="true" /></p>':'').
		'<p><label for="name_user">Votre nom</label><span class="required">*</span><input type="text" id="name_user" name="name_user" value="'.(isset($partie)?htmlentities($partie->get_name(), ENT_COMPAT, 'UTF-8'):'').'" size="30" aria-required="true" /></p>
		<p><label for="date">Date de la partie (JJ/MM/AAAA)</label><span class="required">*</span><input type="text" id="date" name="date" value="'.(isset($partie)?htmlentities($partie->get_date(), ENT_COMPAT, 'UTF-8'):'').'" size="30" aria-required="true" /></p>
		<p><label for="time">Heure de la partie (HH:MM)</label><span class="required">*</span><input type="text" id="time" name="time" value="'.(isset($partie)?htmlentities($partie->get_time(), ENT_COMPAT, 'UTF-8'):'').'" size="30" aria-required="true" /></p>
		<p><label for="email">E-mail</label><span class="required">*</span><input type="text" id="email" name="email" value="'.(isset($partie)?htmlentities($partie->get_email(), ENT_COMPAT, 'UTF-8'):'').'" size="30" aria-required="true"/></p>
		<p><label for="max">Nombre maximum de participants (laisser vide pour pas de limite)</label><input type="text" id="max" name="max" value="'.(isset($partie)?htmlentities($partie->get_max(), ENT_COMPAT, 'UTF-8'):'').'" size="30"/></p>
		<p><label for="comment">Commentaire</label><textarea id="comment" cols="45" rows="8" name="comment">'.(isset($partie)?$partie->get_comment_esc():'').'</textarea></p>
		'.(current_user_can('create_parties')?'<p><label for="officiel">Partie officielle</label> <input type="checkbox" id="officiel" name="officiel" value="1"'.((isset($partie) && $partie->get_officiel())?' checked="checked"':'').'/></p>':''
		).'
		<input type="hidden" name="partie_id" value="'.(isset($partie)?htmlentities ($partie->get_id(), ENT_COMPAT, 'UTF-8'):'').'"/><input type="hidden" name="action" value="add"/>
		<p class="form-submit"><input type="submit" value="Valider"/></p>
		<p>* Champs requis.</p>
		</form>';
}

function bgg_partie_reply_display($atts)
{
	extract( shortcode_atts( array(), $atts ) );
	include_once('class.games.php');
	include_once('class.parties.php');
	$partie_id = isset($_REQUEST['partie_id'])?$_REQUEST['partie_id']:'';
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
	$admin_key = isset($_REQUEST['admin_key'])?$_REQUEST['admin_key']:'';
	if ($admin_key) # affichage du panel d'admin de la réponse
	{
		$reply = Partie_Reply::get_reply_by_admin($admin_key);
		if (!$reply)
		{
			return "<p><strong>Clé d'administration non valide.</strong></p>";
		}
		$partie = Partie::get_partie_by_id($reply->get_partie_id());
		if (!$partie)
		{
			return "<p><strong>Partie non valide.</strong></p>";
		}
		if ($action == "delete" || $action == "delete2")
		{
			if ($action == "delete")
			{
				$output = "<div id=\"plugin-respond\"><p class=\"comment-notes\">Vous êtes sur le point de supprimer votre (".$reply->get_name_esc().") inscription de la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().". ".$partie->get_name_esc()." sera notifié par mail, vous pouvez ajouter un commentaire à la notification en remplissant le champs ci-dessus. Cliquez sur valider pour confirmer la suppression.</p><form method=\"post\" action=\"".get_permalink(get_option('bgg_partie_reply_post'))."?admin_key=".$reply->get_admin_key()."\">
					<input type=\"hidden\" name=\"action\" value=\"delete2\"/>
					<p><label for=\"comment\">Commentaire</label><textarea id=\"comment\" cols=\"45\" rows=\"8\" name=\"comment\"></textarea></p>
					<p class=\"form-submit\"><input type=\"submit\" value=\"Valider\"/></p></form></div>";
			}
			elseif($action == "delete2")
			{
				$comment = isset($_REQUEST['comment'])?$_REQUEST['comment']:'';
				if ($reply->delete())
				{
					$output .= "<p>Votre (".$reply->get_name_esc().") inscription à la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time()." à été supprimée. Un mail a été envoyé à ".$partie->get_name_esc().".</p>";
					$headers = array("From: ".mb_encode_mimeheader(html_entity_decode(get_bloginfo('name'), ENT_COMPAT|ENT_QUOTES|ENT_HTML401))." <".get_option('bgg_mail_from').">");
					wp_mail($partie->get_email(), "L'inscription de ".$reply->get_name()." à la partie de ".$partie->get_game_name()." a été supprimée", "".$reply->get_name()." qui s'était inscrit à la partie de ".$partie->get_game_name()." du ".$partie->get_human_date()." à ".$partie->get_time()." a supprimé son inscription.\n\n".(($comment!='')?'Commentaire : '.$comment:''), $headers);
				}
				else
				{

					$output .= "<p>Impossible de supprimer l'inscription de ".$reply->get_name_esc()." à la partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().".</p>";
				}
			}
			$output .= "<p><a href=\"".get_permalink(get_option('bgg_partie_reply_post'))."?admin_key=".$reply->get_admin_key()."\">Retour</a>";
		}
		else
		{
			$output = "<p>Partie de ".$partie->get_game_link()." du ".$partie->get_human_date()." à ".$partie->get_time().".</p>";
			$output .= "<p>Vous pouvez contacter l'organisateur on lui envoyant un e-mail à l'adresse <a href=\"mailto:".$partie->get_email()."\">".$partie->get_email()."</a>.</p>";
			$output .= "<table><tr><th>Nom</th><th>Commentaire</th></tr>";
			foreach ($partie->get_replies() as $reply2)
			{
				$output .= "<tr><td>".$reply2->get_name_esc()."</td><td>".$reply2->get_comment_esc()."</td></tr>";
			}
			$output .= "</table>";
			$output .= "<p><a href=\"".get_permalink(get_option('bgg_partie_reply_post'))."?admin_key=".$reply->get_admin_key()."&amp;action=delete\">Supprimer votre inscription</a></p>";
		}
		return $output ;
	}
	if (!$partie_id or !is_numeric($partie_id))
	{
		if (current_user_can('manage_options'))  {
			$output .= "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?action=admin\">Administration</a></p>";
		}
		$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';
		$type = isset($_REQUEST['type'])?$_REQUEST['type']:'all';
		return $output.get_next_parties($type, $date);
	}
	$partie = Partie::get_partie_by_id($partie_id);
	if (!$partie)
	{
		return "Partie introuvable.";
	}
	$game_name = $partie->get_game_name();
	$ok = 0;
	if ($action == "add")
	{
		$ok = 1;
		$name_user = stripslashes(isset($_REQUEST['name_user'])?$_REQUEST['name_user']:'');
		$email = stripslashes(isset($_REQUEST['email'])?$_REQUEST['email']:'');
		$comment = stripslashes(isset($_REQUEST['comment'])?$_REQUEST['comment']:'');
		if (empty($name_user))
		{
			$output .= "<p><strong>Tous les champs sont obligatoires.</strong></p>";
			$ok = 0;
		}
		if (!bgg_validate_email($email))
		{
			$output .= "<p><strong>$email n'est pas un email valide.</strong></p>";
			$ok = 0;
		}
		if ($ok == 1)
		{
			$reply = $partie->add_reply($name_user, $email, $comment);
			if ($reply)
			{
				$output .= "<p>Inscription enregistrée.</p>";
				$headers = array('From: '.mb_encode_mimeheader(html_entity_decode(get_bloginfo('name'), ENT_COMPAT|ENT_QUOTES|ENT_HTML401)).' <'.get_option('bgg_mail_from').'>');
				wp_mail($partie->get_email(), "Nouvelle inscription pour la partie de ".$game_name, "Un utilisateur vient de s'inscrire pour votre partie de $game_name.\n\nNom: $name_user\nEmail: $email\nCommentaire: $comment\n\nVoir les inscriptions : ".get_permalink(get_option('bgg_new_partie_post'))."?admin_key=".$partie->get_admin_key(), $headers);
				wp_mail($reply->get_email(), "Votre inscription pour la partie de ".$game_name, "Votre inscription à la partie de $game_name a été prise en compte. Vous pouvez voir les autres participants, ou annuler votre inscription en vous rendant sur le lien suivant : ".get_permalink(get_option('bgg_partie_reply_post'))."?admin_key=".$reply->get_admin_key(), $headers);
			}
			else
			{
				$output .= "<p>Problème lors de l'ajout l'inscription.</p>";
			}

		}
	}
	if ($ok == 0)
	{
		if ($partie->is_full())
		{
			return "<p>Désolé, il ne reste plus de place pour la partie de <strong>".$partie->get_game_name()."</strong> du ".$partie->get_human_date()." à ".$partie->get_time()." organisée par ".$partie->get_name_esc().".</p>";
		}
		if (!$partie->is_open())
		{
			return "<p>Désolé, il n'est plus possible de s'inscrire pour la partie de <strong>".$partie->get_game_name()."</strong> du ".$partie->get_human_date()." à ".$partie->get_time()." organisée par ".$partie->get_name_esc().".</p>";
		}
		$output .= "<p><strong>S'inscrire à la partie de <em>".$partie->get_game_link()."</em> du ".$partie->get_human_date()." à ".$partie->get_time()." organisée par ".$partie->get_name_esc().".</strong><br/> Il y a pour l'instant ".$partie->get_count()." inscrit".($partie->get_count()>1?'s':'').($partie->get_max() > 0?' sur '.$partie->get_max():'').".</p><p>".($partie->get_comment()!=''?'<strong>Commentaire de l\'organisateur :</strong><br/> '.nl2br($partie->get_comment_esc()):'')."</p><div id=\"plugin-respond\"><p class=\"comment-notes\">";
		$output .= get_reply_form($partie->get_id(), $name_user, $email, $comment);
		$output .= "</div>";
	}

	return $output;
}

function get_reply_form($partie_id, $name='', $email='', $comment='')
{
	return '<form action="" method="post">
		<p><label for="name_user">Nom</label><span class="required">*</span><input type="text" id="name_user" name="name_user" value="'.htmlentities($name, ENT_COMPAT, 'UTF-8').'" size="30" aria-required="true" /></p>
		<p><label for="email">E-mail</label><span class="required">*</span> <input type="text" id="email" name="email" value="'.htmlentities($email, ENT_COMPAT, 'UTF-8').'" size="30" aria-required="true" /></p>
		<p><label for="comment">Commentaire</label><textarea id="comment" cols="45" rows="8" name="comment">'.htmlentities($comment,ENT_COMPAT, 'UTF-8').'</textarea></p>
		<input type="hidden" name="partie_id" value="'.htmlentities ($partie_id, ENT_COMPAT, 'UTF-8').'"/><input type="hidden" name="action" value="add"/>
		<p class="form-submit"><input type="submit" value="Valider"/></p>
		<p>* Champs requis.</p>
		</form>';
}

# [bgg_parties type=all/officiel/visiteur date=YYYY-MM-DD count=0]
# count est le nombre de parties à afficher (0 pour pas de limite)
function bgg_parties_display($atts)
{
	extract( shortcode_atts( array('type' => 'all', 'date' => null, 'count' => 0), $atts ) );
	include_once('class.games.php');
	include_once('class.parties.php');
	$output = "";
	if (current_user_can('manage_options'))  {
		$output .= "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."?action=admin\">Administration</a></p>";
	}
	$output .= get_next_parties($type, $date, $count);
	return $output;
}

function get_next_parties($type='all', $date=null, $limit=0, $format='normal')
{
	include_once('class.games.php');
	include_once('class.parties.php');
	if ($date)
	{

		$parties = Partie::get_parties_by_date($type, $date, $limit);
		if (!preg_match("/^\d\d\d\d-\d\d\-\d\d\$/", $date))
		{
			return "Format de date invalide (AAAA-MM-JJ).";
		}
		$date = preg_replace("/^(\d\d\d\d)-(\d\d)-(\d\d)$/", "\\3/\\2/\\1", $date);
		if (count($parties) == 0)
		{
			if ($format=='compact')
				return "";
			return $output."<p>Aucune partie prévue le $date.</p>";
		}
		$output = "Parties prévues le $date :<br/><ul>";
	}
	else
	{
		$parties = Partie::get_parties($type, $limit);
		if (count($parties) == 0)
		{
			if ($format=='compact')
				return "";
			return $output."<p>Aucune partie prévue.</p>";
		}
		$output = "Prochaines parties :<br/><ul class='compact'>";
	}
	foreach ($parties as $partie)
	{
		if ($format == 'compact')
		{
			$output .= "<li".($partie->get_officiel()?" class=\"partie_officielle\"":'')."><a href=\"".get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id()."\">".$partie->get_human_date()." ".$partie->get_time()." : <strong>".$partie->get_game_name()."</strong> (".$partie->get_count().($partie->get_max()>0?"/".$partie->get_max():'').")</a></li>";
		}
		else
		{
			$output .= "<li ".($partie->get_officiel()?"class=\"partie_officielle\"":'')."><a href=\"".get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id()."\">Le ".$partie->get_human_date()." à ".$partie->get_time()." par ".$partie->get_name_esc()." : <strong>".$partie->get_game_name()."</strong> (".$partie->get_count().($partie->get_max()>0?"/".$partie->get_max():'').")</a></li>";
		}
	}
	$output .= "</ul>";
	return $output;
}

function parties_rss()
{
	$tz = date_default_timezone_get();
	date_default_timezone_set(get_option('timezone_string'));
	$title = htmlspecialchars("Organisation de parties");
	$items = "";
	require_once('class.parties.php');
	require_once('class.games.php');
	$type = isset($_REQUEST['type'])?$_REQUEST['type']:'all';
	$parties = Partie::get_parties($type);


	foreach ($parties as $partie)
	{

		$date = $partie->get_creation_date();

		$items .= '<item>'."\n".
			'  <title>Partie de '.htmlspecialchars($partie->get_game_name().' du '.$partie->get_human_date().' à '.$partie->get_time().' ('.$partie->get_count().($partie->get_max()>0?"/".$partie->get_max():'').')')."</title>\n".
			'  <link>'.get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id()."</link>\n".
			'  <pubDate>'.mysql2date("D, d M Y H:i:s O", $date, false)."</pubDate>\n".
			'  <guid>'.get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id()."</guid>\n".
			'  <description>'.htmlspecialchars('Partie de '.$partie->get_game_name().' du '.$partie->get_human_date().' à '.$partie->get_time().' organisée par '.$partie->get_name().'<br/>Nombre d\'inscrit : '.$partie->get_count().($partie->get_max()>0?" sur ".$partie->get_max():'').'<br/>Commentaire : '.$partie->get_comment())."</description>\n".
			'</item>'."\n";

	}
header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;


echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        <?php do_action('rss2_ns'); ?>
>

<channel>
        <title><?php echo $title;?></title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss("description") ?></description>
        <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s O', Partie::get_last_change_date(), false); ?></lastBuildDate>
        <language><?php echo get_option('rss_language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '6' ); ?></sy:updateFrequency>
        <?php do_action('rss2_head'); ?>
	<?php echo $items;?>
	</channel>
</rss>
<?php
	date_default_timezone_set($tz);
}

class Parties_Widget extends WP_Widget
{
	function __construct() {
		parent::WP_Widget( /* Base ID */'parties_widget', /* Name */'Parties_Widget', array( 'description' => 'Widget affichant les prochaines parties' ) );
	}

	function widget($args, $instance)
	{
		extract ($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		$count = $instance['count'];
		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;
		$output = get_next_parties('all', null, $count, 'compact');
		if ($output == "")
			echo "<p><em>Il n'y a aucune partie de prévue</em></p>";
		else
			echo $output;
		echo $instance['text_after'];
//		echo "<p><a href=\"".get_permalink(get_option('bgg_new_partie_post'))."\">Organiser une nouvelle partie</a></p>";
		echo $after_widget;
	}
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = (is_numeric($new_instance['count'])?$new_instance['count']:$old_instance['count']);
		$instance['text_after'] = $new_instance['text_after'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance ) {
		if ( $instance ) {
			$title = esc_attr( $instance[ 'title' ] );
			$count = esc_attr( $instance[ 'count' ] );
			$text_after = esc_attr( $instance[ 'text_after' ] );
		}
		else {
			$title = __( 'New title', 'text_domain' );
			$count = "5";
			$text_after = "";
		}
?>
		<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id('count'); ?>">Nombre de parties</label> 
		<input class="widefat" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" type="text" value="<?php echo $count; ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id('text_after'); ?>">Texte</label> 
		<textarea class="widefat" id="<?php echo $this->get_field_id('text_after'); ?>" name="<?php echo $this->get_field_name('text_after'); ?>"><?php echo $text_after; ?></textarea>
		</p>
<?php 
	}

}

// supprime les entrées de la BGG correspondant à l'utilisateur lors de la 
// suppression de l'utilisateur par wordpress
function bgg_clean_user($user_id)
{
	global $wpdb;
	$user_info = get_userdata($user_id);
	if (!$user_info)
		return;
	$username = $user_info->user_login;
	//sauvegarde des listes des jeux avant suppression
	//backups/username-YYYYMMDD_HHMMSS
	if (!file_exists(WP_CONTENT_DIR."/backups"))
	{
		mkdir(WP_CONTENT_DIR."/backups", 0755, true);
	}
	$filename = WP_CONTENT_DIR."/backups/".$username."-".date("Ymd_His");
	$query = $wpdb->prepare("SELECT game_id,type FROM `".$wpdb->prefix."games_by_list` WHERE  list_id=%s", $username);
	$results = $wpdb->get_results($query);
	$fp = fopen($filename, "w");
	foreach ($results as $row)
	{
		fwrite($fp, $row->type.",".$row->game_id."\n");
	}
	fclose($fp);
	// on supprime les données
	$query=$wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_users` WHERE id=%s", $username);
	$wpdb->query($query);
	$query=$wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_userlists` WHERE id=%s", $username);
	$wpdb->query($query);
	$query=$wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_by_list` WHERE (type='usercoeur' or type='userbest' or type='user') AND list_id=%s", $username);
	$wpdb->query($query);
}

function bgg_validate_email($email)
{

	if (is_email($email))
	{
		list($prefix, $domain) = split("@",$email);
		if(function_exists("getmxrr") && getmxrr($domain, $mxhosts))
		{
			return true;
		}
	}
	return false;

}

function parties_lastpostmodified($val, $timezone = 'server')
{
	include_once("class.parties.php");
	$lastpost = Partie::get_last_change_date();
	return ($lastpost > $val ? $lastpost : $val);
}
function coeur_lastpostmodified($val, $timezone = 'server')
{
	include_once("class.games.php");
	$lastpost = Games::get_last_coeur_date();
	return ($lastpost > $val ? $lastpost : $val);
}

function bgg_init()
{
	add_feed('parties', 'parties_rss');
	add_feed('coeur', 'coeur_rss');
	add_filter('get_lastpostmodified', 'parties_lastpostmodified');
	add_filter('get_lastpostmodified', 'coeur_lastpostmodified');
}

function wp_bgg_admin_bar() {
	    global $wp_admin_bar, $wpdb;
	        if ( !current_user_can('manage_options') || !is_admin_bar_showing() )
			        return;
		    $wp_admin_bar->add_menu( array( 'id' => 'bgg_admin_menu', 'title' => 'BGG', 'href' => '/wp-admin/'.wp_nonce_url( 'tools.php?page=bgg-options-id', 'wp-bgg' ) ) );
}

add_action( 'admin_bar_menu', 'wp_bgg_admin_bar', 900 );

add_shortcode('bgg', 'bgg_display');
add_shortcode('bgg_images', 'bgg_images_display');
add_shortcode('bgg_explications', 'bgg_explications_display');
add_shortcode('bgg_user_update', 'bgg_user_update_display');
add_shortcode('bgg_new_partie', 'bgg_new_partie_display');
add_shortcode('bgg_partie_reply', 'bgg_partie_reply_display');
add_shortcode('bgg_parties', 'bgg_parties_display');
add_shortcode('bgg_game', 'bgg_game_display');
register_activation_hook(__FILE__,'bgg_install');
add_filter('wp_title', 'bgg_title');
add_action('init', 'bgg_init');
add_action('widgets_init', create_function('', 'return register_widget("Parties_Widget");'));
add_action('delete_user', 'bgg_clean_user');

?>
