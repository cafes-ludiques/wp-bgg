<?php
class Partie
{
	private $id, $game_id, $game_name, $name, $date, $time, $officiel, $admin_key, $max, $comment, $email, $creation_date;
	function __construct($id, $game_id, $game_name, $name, $date, $time, $max, $comment, $email, $officiel = 0, $admin_key="", $creation_date=0)
	{
		$this->id = $id;
		$this->game_id = $game_id;
		$this->game_name = $game_name;
		$this->name = $name;
		$this->date = $date;
		$this->time = $time;
		$this->max = $max;
		$this->comment = $comment;
		$this->email = $email;
		$this->officiel = $officiel;
		$this->admin_key = $admin_key;
		$this->creation_date = $creation_date;
		if ($this->admin_key == "")
		{
			$this->admin_key = uniqid();
		}
	}
	static function get_last_change_date() # récupère la date de la dernière entrée en BDD (replies et parties confondu)
	{
		global $wpdb;
		$query = "SELECT creation_date FROM `".$wpdb->prefix."parties` ORDER BY creation_date DESC LIMIT 1";
		$partie_last = $wpdb->get_var($query);
		$query = "SELECT date FROM `".$wpdb->prefix."parties_replies` ORDER BY date DESC LIMIT 1";
		$reply_last = $wpdb->get_var($query);
		return ($reply_last>$partie_last?$reply_last:$partie_last);

	}

	static function get_partie_by_id($id)
	{
		global $wpdb;
		$query = "SELECT game_id, game_name, user, DATE(date) as date2, TIME(date) as time, max, comment, email, officiel, admin_key, creation_date FROM `".$wpdb->prefix."parties` WHERE id='$id'";
		$row = $wpdb->get_row($query);
		if ($row)
		{
			return new Partie($id, $row->game_id, $row->game_name, $row->user, $row->date2, $row->time, $row->max, $row->comment, $row->email, $row->officiel, $row->admin_key, $row->creation_date);
		}
	}
	static function get_partie_by_admin($admin_key)
	{
		global $wpdb;
		$query = "SELECT id, game_id, game_name, user, DATE(date) as date2, TIME(date) as time, max, comment, email, officiel, creation_date FROM `".$wpdb->prefix."parties` WHERE admin_key='$admin_key'";
		$row = $wpdb->get_row($query);
		if ($row)
		{
			return new Partie($row->id, $row->game_id, $row->game_name, $row->user, $row->date2, $row->time, $row->max, $row->comment, $row->email, $row->officiel, $admin_key, $row->creation_date);
		}
	}
	static function get_parties($type='all', $limit=0)
	{
		global $wpdb;
		$query = "SELECT id, game_id, game_name, user, DATE(date) as date2, TIME(date) as time, max, comment, email, officiel, admin_key, creation_date FROM `".$wpdb->prefix."parties` WHERE date >= NOW()";
		if ($type=='officiel')
		{
			$query .= " AND officiel='1'";
		}
		elseif ($type=='visiteur')
		{
			$query .= " AND officiel='0'";
		}
		$query .= " ORDER BY date";
		if (is_numeric($limit) && $limit != 0)
			$query .= " LIMIT $limit";
		$res = $wpdb->get_results($query);
		$output = array();
		foreach ($res as $row)
		{
			array_push($output,  new Partie($row->id, $row->game_id, $row->game_name, $row->user, $row->date2, $row->time, $row->max, $row->comment, $row->email, $row->officiel, $row->admin_key, $row->creation_date));
		}
		return $output;
	}
	function get_parties_by_date($type='all', $date, $limit=0)
	{
		global $wpdb;
		$query = "SELECT id, game_id, game_name, user, DATE(date) as date2, TIME(date) as time, max, comment, email, officiel, admin_key, creation_date FROM `".$wpdb->prefix."parties` WHERE DATE(date) = '$date'";
		if ($type=='officiel')
		{
			$query .= " AND officiel='1'";
		}
		elseif ($type=='visiteur')
		{
			$query .= " AND officiel='0'";
		}
		$query .= " ORDER BY date";
		if (is_numeric($limit) && $limit != 0)
			$query .= " LIMIT $limit";
		$res = $wpdb->get_results($query);
		$output = array();
		foreach ($res as $row)
		{
			array_push($output,  new Partie($row->id, $row->game_id, $row->game_name, $row->user, $row->date2, $row->time, $row->max, $row->comment, $row->email, $row->officiel, $row->admin_key, $row->creation_date));
		}
		return $output;
	}
	static function get_parties_by_game_id($type='all', $game_id)
	{
		global $wpdb;
		$query = "SELECT id, user, DATE(date) as date2, TIME(date) as time, max, comment, email, officiel, admin_key FROM `".$wpdb->prefix."parties` WHERE game_id='$game_id' AND date >= NOW()";
		if ($type=='officiel')
		{
			$query .= " AND officiel='1'";
		}
		elseif ($type=='visiteur')
		{
			$query .= " AND officiel='0'";
		}
		$query .= " ORDER BY date";
		if (is_numeric($limit) && $limit != 0)
			$query .= " LIMIT $limit";
		$res = $wpdb->get_results($query);
		$output = array();
		foreach ($res as $row)
		{
			array_push($output,  new Partie($row->id, $game_id, '', $row->user, $row->date2, $row->time, $row->max, $row->comment, $row->email, $row->admin_key, $row->officiel));
		}
		return $output;
	}
	function get_id()
	{
		return $this->id;
	}
	function get_game_id()
	{
		return $this->game_id;
	}
	function get_game_name()
	{
		include_once("class.games.php");
		if ($this->game_id)
		{
			return Games::get_game_name($this->game_id);
		}
		else
		{
			return $this->game_name;
		}
	}
	function get_game_name_esc()
	{
		return htmlentities($this->get_game_name(), ENT_COMPAT, 'UTF-8');
	}
	function get_game_link()
	{
		if ($this->game_id)
		{

			return "<a href='".get_permalink(get_option('bgg_post_id'))."?game_id=".$this->get_game_id()."'>".$this->get_game_name_esc()."</a>";
		}
		else
		{
			return $this->get_game_name_esc();
		}
	}
	function get_admin_key()
	{
		return $this->admin_key;
	}
	function get_name()
	{
		return $this->name;
	}
	function get_name_esc()
	{
		return htmlentities($this->name, ENT_COMPAT, 'UTF-8');
	}
	function get_email()
	{
		return $this->email;
	}
	function get_email_esc()
	{
		return htmlentities($this->email, ENT_COMPAT, 'UTF-8');
	}
	function get_date()
	{
		return $this->date;
	}
	function set_date($date)
	{
		$this->date = $date;
	}
	function get_human_date()
	{
		$tmp = explode('-', $this->date);
		return $tmp[2]."/".$tmp[1]."/".$tmp[0];
	}
	function get_time()
	{
		return substr($this->time,0,5);
	}
	function get_max()
	{
		return $this->max;
	}
	function get_officiel()
	{
		return $this->officiel;
	}
	function get_comment()
	{
		return $this->comment;
	}
	function get_comment_esc()
	{
		return htmlentities($this->comment, ENT_COMPAT, 'UTF-8');
	}

	function get_count()
	{
		if (!isset($this->count))
		{
			global $wpdb;
			$query = $wpdb->prepare("SELECT count(*) as c FROM `" . $wpdb->prefix . "parties_replies` WHERE partie_id=%s", $this->id);
			$this->count = $wpdb->get_var($query);
		}
		return $this->count;
	}

	function is_full()
	{

		return ($this->get_max() > 0 and $this->get_max() == $this->get_count()); 
	}

	function is_open() # on vérifie pour l'instant uniquement si la date est encore valable.
	{
		return ($this->get_date()."T".$this->get_time() > substr(date("c"),0,16));
	}

	function get_creation_date()
	{
		return $this->creation_date;
	}

	function save()
	{
		global $wpdb;
		if ($this->get_id() == 0)
		{
			$query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."parties` VALUES (NULL, %d, %s, %s, %s, %d, %s, %s, %s, %s, NOW())", $this->game_id, $this->game_name, $this->name, $this->date." ".$this->time, $this->max, $this->officiel, $this->admin_key, $this->comment, $this->email);
			$ret = $wpdb->query($query);
			$this->id = mysql_insert_id();
			return $ret;
		}
		else
		{
			$query = $wpdb->prepare("UPDATE `".$wpdb->prefix."parties` SET game_id=%d, game_name=%s, name=%s, date=%s, max=%d, officiel=%s, admin_key=%s, comment=%s, email=%s WHERE id=%d", $this->game_id, $this->game_name, $this->name, $this->date." ".$this->time, $this->max, $this->officiel, $this->admin_key, $this->comment, $this->email, $this->id);
			return $wpdb->query($query);
		}
	}

	function get_replies()
	{
		if (!isset($this->replies))
		{
			$this->replies = array();
			global $wpdb;
			$query = $wpdb->prepare("SELECT *, DATE_FORMAT(date, '%%d/%%m/%%Y %%T') as date2 FROM `" . $wpdb->prefix . "parties_replies` WHERE partie_id=%d ORDER BY user", $this->id);
			$res = $wpdb->get_results($query);
			foreach ($res as $row)
			{
				array_push($this->replies, new Partie_Reply($row->user, $row->partie_id, $row->email, $row->comment, $row->id, $row->date2, $row->admin_key));
			}
		}
		return $this->replies;
	}

	function delete()
	{
		// suppression des réponses
		foreach ($this->get_replies() as $reply)
		{
			$reply->delete();
		}
		global $wpdb;
		$query = $wpdb->prepare("DELETE FROM  `".$wpdb->prefix."parties` WHERE id=%s", $this->id);
		return $wpdb->query($query);
	}

	function add_reply($name, $email, $comment)
	{
		global $wpdb;
		$admin_key = uniqid();
		$query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."parties_replies` VALUES (NULL, %d, %s, %s, NOW(), %s, %s)", $this->get_id(), $name, $email, $comment, $admin_key);
		if ($wpdb->query($query))
		{
			$id = mysql_insert_id();
			return new Partie_Reply($name, $this->get_id(), $email, $comment, $id, '', $admin_key);
		}
		else
		{
			return False;
		}
	}
}


class Partie_Reply
{
	public $id, $partie_id, $name, $email, $date, $comment, $admin_key;
	function __construct($name, $partie_id, $email, $comment, $id='', $date='', $admin_key='')
	{
		$this->id=$id;
		$this->name = $name;
		$this->partie_id = $partie_id;
		$this->email = $email;
		$this->comment = $comment;
		$this->date=$date;
		if (!$this->date)
		{
			$this->date=strftime("%d/%m/%y %T");
		}
		$this->admin_key=$admin_key;
	}

	static public function get_reply_by_id($id)
	{
		global $wpdb;
		$query = $wpdb->prepare("SELECT *,DATE_FORMAT(date, '%%d/%%m/%%Y %%T') as date2 FROM `".$wpdb->prefix."parties_replies` WHERE id=%d", $id);
		$row = $wpdb->get_row($query);
		if ($row)
		{
			return new Partie_Reply($row->user, $row->partie_id, $row->email, $row->comment, $id, $row->date2, $row->admin_key);
		}
		else
		{
			return False;
		}
	}
	static public function get_reply_by_admin($admin_key)
	{
		global $wpdb;
		$query = $wpdb->prepare("SELECT *,DATE_FORMAT(date, '%%d/%%m/%%Y %%T') as date2 FROM `".$wpdb->prefix."parties_replies` WHERE admin_key=%s", $admin_key);
		$row = $wpdb->get_row($query);
		if ($row)
		{
			return new Partie_Reply($row->user, $row->partie_id, $row->email, $row->comment, $row->id, $row->date2, $admin_key);
		}
		else
		{
			return False;
		}
	}
	function get_id()
	{
		return $this->id;
	}
	function get_partie_id()
	{
		return $this->partie_id;
	}
	function get_name()
	{
		return $this->name;
	}
	function get_name_esc()
	{
		return htmlentities($this->name, ENT_COMPAT, 'UTF-8');
	}
	function get_comment()
	{
		return $this->comment;
	}
	function get_comment_esc()
	{
		return htmlentities($this->comment, ENT_COMPAT, 'UTF-8');
	}
	function get_email()
	{
		return $this->email;
	}
	function get_admin_key()
	{
		return $this->admin_key;
	}

	public function delete()
	{
		global $wpdb;
		$query = $wpdb->prepare("DELETE FROM  `".$wpdb->prefix."parties_replies` WHERE id=%d", $this->id);
		return $wpdb->query($query);
	}
}

?>
