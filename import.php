<?php 
exit;
header('Content-Type: text/html; charset=UTF-8');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml
/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
  <title>Outils - Moi j'm'en fous je triche - Import</title>
  </head>
  <body>
<?php

include('../../../conf/config.php');
include('functions.php');
include('../../../inc/classes/class.mysql.php');
$con = new Connection(DB_USER, DB_PASS, DB_HOST, DB_DBASE);
if ($con->error())
{
       print "<p>Impossible de se connecter &agrave; la base</p>";
       print $con->error();
       exit;
}

$list_id = $_REQUEST['list_id']?$_REQUEST['list_id']:'';
$begin = $_REQUEST['begin']?$_REQUEST['begin']:0;

if (is_numeric($list_id))
{
  if ($begin == 0)
  {
    #$con->execute("DELETE FROM mech USING `".DB_PREFIX."games_mechanics` as mech, `".DB_PREFIX."games` as g WHERE g.list_id='".mysql_real_escape_string($list_id)."' AND g.id = mech.game_id");
    #$con->execute("DELETE FROM cat USING `".DB_PREFIX."games_categories` as cat, `".DB_PREFIX."games` as g WHERE g.list_id='".mysql_real_escape_string($list_id)."' AND g.id = cat.game_id");
    #$con->execute("DELETE FROM designer USING `".DB_PREFIX."games_designers` as designer, `".DB_PREFIX."games` as g WHERE g.list_id='".mysql_real_escape_string($list_id)."' AND g.id = designer.game_id");
    #$con->execute("DELETE FROM artist USING `".DB_PREFIX."games_artists` as artist, `".DB_PREFIX."games` as g WHERE g.list_id='".mysql_real_escape_string($list_id)."' AND g.id = artist.game_id");
    #$con->execute("DELETE FROM pub USING `".DB_PREFIX."games_publishers` as pub, `".DB_PREFIX."games` as g WHERE g.list_id='".mysql_real_escape_string($list_id)."' AND g.id = pub.game_id");
    $con->execute("DELETE FROM `".DB_PREFIX."games_by_list` WHERE list_id='".mysql_real_escape_string($list_id)."_temp'");
  }
  $query = "SELECT type, bggid FROM `".DB_PREFIX."games_lists` WHERE id='".mysql_real_escape_string($list_id)."'";
  $res = $con->select($query);
  if ($res->fetch())
  {
    $last=Games::import_list_items($list_id."_temp", $res->f('type'), $res->f('bggid'),$begin,20);
    if (is_numeric($last))
    {
      header('Location: '.$_SERVER['SCRIPT_URI']."?list_id=$list_id&begin=$last");
      echo "Import en cours : $last jeux importés";
    }
    else { 
      $con->execute("DELETE FROM `".DB_PREFIX."games_by_list` WHERE list_id='".mysql_real_escape_string($list_id)."'");
      $con->execute("UPDATE `".DB_PREFIX."games_by_list` SET list_id='".mysql_real_escape_string($list_id)."' WHERE list_id='".mysql_real_escape_string($list_id)."_temp'");
      echo "Import terminé";
    }
  }
}

?>
