<?php 
require_once ('class.bgg.php');
require_once ('class.game.php');
class Games
{
  static function get_upload_dir()
  {
    return WP_CONTENT_DIR . '/uploads/bgg/';
  }

  function display_array($array, $op)
  {
    if (!is_array($array))
    {
      return $array;
    }
    if (count($array) == 1)
    {
      return $array[0];
    }
    $last = array_pop($array);
    $output = implode(", ", $array);
    if ($op == "AND")
    {
      $output .= " et ";
    }
    else
    {
      $output .= " ou ";
    }
    $output .= $last;
    return $output;
  }

  static function get_location($loc_type)
  {
    switch($loc_type)
    {
      case 'xs':
        return 'XS';
      case 's':
        return 'S';
      case 'm':
        return 'M';
      case 'l':
        return 'L';
      case 'ludo':
        return 'Ludopathèque';
      case 'party':
        return 'Ambiance';
      case '2':
        return 'Jeux&nbsp;à&nbsp;deux';
      case 'famille':
        return 'Famille';
      case 'adresse':
        return 'Adresse';
      case 'traduire':
        return 'A traduire';
      case 'lettres':
        return 'Lettres';
      case 'coop':
        return 'Jeux&nbsp;coopératifs';
      case 'vitrine':
        return 'Vitrine';
      case 'sac':
        return 'Sac';
      case 'wargame':
        return 'Wargame';
      default:
        return '';
    }
  }

  static function display_persons_list($text, $type, $array, $base_uri)
  {
    $nb = count($array);
    $output = "";
    if ($nb > 0)
    {
      if ($nb > 1)
      {
        $output .= "<li>".$text."s";
      }
      else
      {
        $output .= "<li>".$text;
      }
      $output .= "<ul>";
      foreach ($array as $name=>$id)
      {
        $output .= "<li><a href='".$base_uri."&search&".$type."=".$id."'>".$name."</a></li>";
      }
      $output .= "</ul></li>";
    }
    return $output;
  }

  function get_game_name($game_id)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT name FROM `".$wpdb->prefix."games` WHERE id=%d", $game_id);
    return $wpdb->get_var($query);
  }
  function get_list_infos($list_id)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT name, title, comment FROM `".$wpdb->prefix."games_lists` WHERE id=%d", $game_id);
    $row = $wpdb->get_row($query);
    $ret = array();
    if ($row)
    {
      $ret['title'] = $row->title;
      $ret['name'] = $row->name;
      $ret['comment'] = $row->comment;
    }
    return $ret;
  }

  function games_images($list, $limit=0, $new=false)
  {
    if (!is_numeric($limit)) { die("limit doit êst un nombre");}
	    global $wpdb;
    $out = "";
    $query = $wpdb->prepare("SELECT games.name,games.thumbnail,games.id,list.list_id FROM `".$wpdb->prefix."games` as games,`".$wpdb->prefix."games_by_list` as list WHERE list.type='list' AND games.id=list.game_id AND list.list_id=%s", $list);
    if ($new)
    { 
      $query.=" AND games.new='$new'";
    }
    $query .= " ORDER BY RAND() LIMIT $limit";
    $res = $wpdb->get_results($query);
    $out .= '<ul class="new_games">'."\n";
    foreach ($res as $row)
    {
      $game = new Game($row);
      $out .= '<li><a href="'.$game->get_link().'"><img src="'. $game->get_thumbnail().'" alt="'.$game->get_name().'" title="'.$game->get_name().'" /></a></li>'."\n";
    }
    $out .= "</ul>\n";
    $out .= "<div style=\"clear:both;height:0px;\">&nbsp;</div>\n";
    return $out;
  }

  static function import_list_items($list_id, $type, $bggid)
  {
    global $wpdb;
    $bgg = new BGG();
    switch ($type)
    {
    case 'collection':
      $array = $bgg->get_collection($bggid);
      break;
    case 'geeklist':
      $array = $bgg->get_geeklist($bggid);
      $query = $wpdb->prepare("UPDATE `".$wpdb->prefix."games_lists` SET title=%s, comment=%s WHERE id=%s", $array[0]['geeklisttitle'], $array[0]['geeklistcomment'], $list_id);
      $wpdb->query($query);


      break;
    default:
      throw new Exception("Type de liste invalide");
      break;
    }
    try
    {
      foreach ($array as $entry)
      {
	# télécharge la miniature si ce n'est pas déjà fait
	$tn_filename = basename($entry['thumbnail']);
	if ($tn_filename == '')
	{
		$tn_filename = "default.png";
	}
	else
	{
		if (!file_exists(Games::get_upload_dir().$tn_filename) or filesize(Games::get_upload_dir().$tn_filename) == 0)
		{
			file_put_contents(Games::get_upload_dir().$tn_filename, file_get_contents($entry['thumbnail']));
			if (filesize(Games::get_upload_dir().$tn_filename) == 0)
			{
				$tn_filename = "default.png";
			}
		}
	}
	if ($type == 'collection' && Games::get_location(trim(strtolower($entry['wishlistcomment']))) == '') 
	{
		echo "Ignore <a href='http://www.boardgamegeek.com/boardgame/".$entry['objectid']."'>".$entry['name']."</a>, wishlist comment ".trim(strtolower($entry['wishlistcomment']))." invalide<br/>";
		continue;
	}
	$query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games` VALUES (%s, %s, %s, %s, %s, '', '', '', '', %s, %s, %s, '', '', '', '') ON DUPLICATE KEY UPDATE name=VALUES(name), thumbnail=VALUES(thumbnail), bggid=VALUES(bggid), comment=VALUES(comment), publisher=VALUES(publisher), location=VALUES(location), new=VALUES(new)", $entry['id'], $entry['name'], $tn_filename, $entry['objectid'], $entry['comment'], $entry['publisher'], trim(strtolower($entry['wishlistcomment'])), $entry['want_to_play']);
	$wpdb->show_errors();
	if ($wpdb->query($query) === FALSE)
	{
		echo "Erreur avec la requête $query :";
		$wpdb->print_error(); 
		exit;
	}
	$query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_by_list` VALUES (%s, %s, 'list')", $entry['id'], $list_id);
	$wpdb->query($query);
      }
    }
    catch (Exception $e)
    {
      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
  }

  static function update_games_info($list_id)
  {
    global $wpdb;
    $bgg = new BGG();
    $query = $wpdb->prepare("SELECT game_id,bggid, name FROM `".$wpdb->prefix."games` as g, `".$wpdb->prefix."games_by_list` as gl WHERE g.id=gl.game_id AND list_id=%s AND type='list' AND (TO_DAYS(NOW()) - TO_DAYS(last_update) > 30 OR last_update='0000-00-00 00:00:00')", $list_id);
    $res = $wpdb->get_results($query);
    $i=0;
    $last_slice=0;
    $ext_infos_arr;
    try
    {
      foreach ($res as $row)
      {
        $game_id = $row->game_id;
        $bgg_id = $row->bggid;
        $i++;
        if ($i>$last_slice)
        {
          set_time_limit(30);
          $games_arr=array();
          foreach (array_slice($res, $last_slice, 40) as $game)
          {
            array_push($games_arr, $game->bggid);
          }
	  $ext_infos_arr = $bgg->get_game_extended_info($games_arr);
          $last_slice+=40;
        }

	$ext_infos = $ext_infos_arr[$bgg_id];
	$alternate_names = implode(", ", array_diff($ext_infos['name'], array($row->name)));
	$query = $wpdb->prepare("UPDATE `".$wpdb->prefix."games` SET minplayers=%d, maxplayers=%d, age=%d, length=%d, bestplayers=%s, sugplayers=%s, altnames=%s, last_update=NOW() WHERE id=%d", $ext_infos['minplayers'], $ext_infos['maxplayers'], $ext_infos['age'], $ext_infos['length'], implode(",", $ext_infos['bestplayers'][1]), implode(",", $ext_infos['bestplayers'][0]), $alternate_names, $game_id);
	$wpdb->show_errors();
	if ($wpdb->query($query) === FALSE)
	{
		echo "Erreur avec la requête $query :";
		$wpdb->print_error(); 
		exit;
	}
	// on supprime tous les mech/cat/designer/artist du jeu
	$query = $wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_mechanics` WHERE game_id=%d", $game_id);
	$wpdb->query($query);
	$query = $wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_categories` WHERE game_id=%d", $game_id);
	$wpdb->query($query);
	$query = $wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_designers` WHERE game_id=%d", $game_id);
	$wpdb->query($query);
	$query = $wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_artists` WHERE game_id=%d", $game_id);
	$wpdb->query($query);
        foreach ($ext_infos['mechanic'] as $mecha)
        {
          $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_mechanics` VALUES (%d, %d)", $mecha, $game_id);
          $wpdb->query($query);
        }
        foreach ($ext_infos['category'] as $cat)
        {
          $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_categories` VALUES (%d, %d)", $cat, $game_id);
          $wpdb->query($query);
        }
        foreach ($ext_infos['designer'] as $designer)
        {
          $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_designers` VALUES (%d, %d)", $designer, $game_id);
          $wpdb->query($query);
        }
        foreach ($ext_infos['artist'] as $artist)
        {
          $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_artists` VALUES (%d, %d)", $artist, $game_id);
          $wpdb->query($query);
        }
      }
    }
    catch (Exception $e)
    {
      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    //recupere les editeurs non présent dans la BDD
    $query = "SELECT DISTINCT g.publisher as pub_id from `".$wpdb->prefix."games` as g left join `".$wpdb->prefix."games_publishers_name` as p ON p.id=g.publisher WHERE p.name is NULL AND g.publisher != 0";
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      $pub_id = $row->pub_id;
      try
      {
        $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_publishers_name` VALUES (%d, %s)", $pub_id, $bgg->get_publisher_name($pub_id));
        $wpdb->query($query);
      }
      catch (Exception $e)
      {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
      }
    }
    //recupere les auteurs non présent dans la BDD
    $query = "select distinct t.id as pers_id from (select id from `".$wpdb->prefix."games_artists` union select id from `".$wpdb->prefix."games_designers`) as t where t.id not in (select id from `".$wpdb->prefix."games_people_name`);";
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      $pers_id = $row->pers_id;
      try
      {
        $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_people_name` VALUES (%d, %s)", $pers_id, $bgg->get_person_name($pers_id));
        $wpdb->query($query);
      }
      catch (Exception $e)
      {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
      }
    }
  }

  function display_game($id)
  {
    global $wpdb;
    $base_uri = get_permalink()."?list_id=".urlencode(get_option('bgg_default_list'));
    $output = "";
    $game = Game::from_id($id);
    if (!$game)
    {
      return "<p>Jeu introuvable</p>";
    }
    $output .= "<h3>".$game->get_name()."</h3>\n";
    if ($game->get_comment())
    {
      $output .= "<p>".str_replace(array("\r\n", "\n", "\r"), '<br/>', $game->get_comment())."</p>\n";
    }
    $medal = Games::get_medal($game->get_id(), 'medium');
    if ($medal)
    {
	    $medal .= "<br/>";
    }
    $output .= "<div style='text-align: center; float: right;'>$medal<img src='".$game->get_thumbnail()."' id='game_thumb'/></div>\n";
    $output .= "<ul>\n";
    $output .= "<li>".$game->get_players()." joueurs</li>\n";
    if ($game->get_sugplayers())
    {
      $sug = explode(",", $game->get_sugplayers());
      $best = explode(",", $game->get_bestplayers());
      // on met en gras les jeux "best for x players"
      array_walk($sug, create_function('&$item, $key, $best', 'if (in_array($item, $best)){ $item="<strong style=\"color: #FF8106;\">".$item."</strong>";}'), $best);
      $output .= "<li>Conseillé pour ".implode(', ', $sug)." joueurs.</li>";
    }
    $output .= "<li>".$game->get_age()." ans et plus</li>\n";
    $output .= "<li>".$game->get_length()." minutes</li>\n";
    if ($game->get_location())
    {
      $output .= "<li>Emplacement&nbsp;: ".Games::get_location($game->get_location())."</li>\n";
    }
    if ($game->get_altnames())
    {
      $output .= "<li>Autres noms&nbsp;: ".$game->get_altnames()."</li>\n";
    }
    
    $output .= Games::display_persons_list("Auteur", "designers", $game->get_designers(), $base_uri);
    $output .= Games::display_persons_list("Illustrateur", "artists", $game->get_artists(), $base_uri);
    $output .= Games::display_persons_list("Mécanisme", "mechanics", $game->get_mechanics(), $base_uri);
    $output .= Games::display_persons_list("Catégorie", "categories", $game->get_categories(), $base_uri);

    if ($game->get_pubname() != '')
    {
      $output .= "<li>Publié par : <a href='".$base_uri."&search&publisher=".$game->get_pubid()."'>".$game->get_pubname()."</a></li>";
    }
    $output .= "</ul>";

    // actif si plugin benevoles actif
    if (function_exists('trombi_get_url'))
    {
	    $query = "SELECT DISTINCT list_id FROM `".$wpdb->prefix."games_by_list` as gl  WHERE gl.game_id='".$game->get_id()."' AND (gl.type='userbest' OR gl.type='usercoeur') ORDER BY list_id";
	    $res = $wpdb->get_results($query);
	    if ($wpdb->num_rows > 0)
	    {
	      $output .= "<strong>Ce jeu est un des jeux préférés de</strong> : <ul>\n";
	      foreach ($res as $row)
	      {
		$user_info = get_user_by('login', $row->list_id);
	        $output .= "<li><a href=\"".trombi_get_url()."?id=".$row->list_id."\">".$user_info->display_name."</a></li>";
	      }
	      $output .= "</ul>\n";
	    }
	    $query = "select list_id as id FROM `".$wpdb->prefix."games_by_list` as list  WHERE list.game_id='".$game->get_id()."' AND list.type='user' ORDER BY list_id";
	    $res = $wpdb->get_results($query);
	    if ($wpdb->num_rows > 0)
	    {
	      $output .= "<strong>Ce jeu peut vous être expliqué par</strong> : <ul>\n";
	      foreach ($res as $row)
	      {
		$user_info = get_user_by('login', $row->id);
	        $output .= "<li><a href=\"".trombi_get_url()."?id=".$row->id."\">".$user_info->display_name."</a></li>";
	      }
	      $output .= "</ul>\n";
	    }
    }
    $output .= "<p><a href='".get_permalink(get_option('bgg_new_partie_post'))."?game_id=".$game->get_id()."'>Organiser une partie de ce jeu</a></p>\n";
    include_once('class.parties.php');
    $parties = Partie::get_parties_by_game_id('all', $game->get_id());
    if (count($parties) > 0)
    {
	    $output .= "<p>Les prochaines parties organisées pour ce jeu :</p>";
	    $output .= "<ul>";
	    foreach ($parties as $partie)
	    {
              $output .= "<li><a href=\"".get_permalink(get_option('bgg_join_partie_post'))."?partie_id=".$partie->get_id()."\">Le ".$partie->get_human_date()." à ".$partie->get_time()." par ".$partie->get_name_esc()." (".$partie->get_count().($partie->get_max()>0?"/".$partie->get_max():'').")</a></li>";
	    }
	    $output .= "</ul>";

    }
    $output .= "<p>Les informations de cette page proviennent de <a href=\"http://boardgamegeek.com/boardgame/".$game->get_bggid()."\">BoardGameGeek</a></p>";
    $output .= "<p><a href='".$base_uri."'>Retour</a></p>";
    return $output;
  }

  function display_list($res,$list_id, $compact=0, $type='list')
  {
	  $output = "";
	  $count = count($res);
	  if ($compact == 0) {
		  $output .= "<p>Il y a ".$count." jeu";
		  if ($count > 1) $output .= "x";
		  $output .= " dans cette liste.</p>\n";
	  }
	  else
	  {
		  $output .= "<!-- nb_items=".$count." -->";
	  }
	  $output .= "<table class='list_jeux'>";
    if ($compact == 0 || $compact == 2)
      $output .= "<tr><th></th><th>Nom</th><th>Joueurs</th><th>Durée</th><th>Age</th><th>Emplacement</th></tr>";
    $i=0;
    foreach ($res as $row)
    {
      $game = new Game($row);
      if ($i==0)
      {
        $output .= "<tr class='list_jeux_impair'>";
        $i=1;
      }
      else
      {
        $output .= "<tr class='list_jeux_pair'>";
        $i=0;
      }

      $output .= "<td>";
      if (isset($row->coeur)) // vrai si on affiche la page d'un user
      {
	      if ($row->coeur)
	      {
		      $output .= "<img src=\"".get_template_directory_uri()."/images/coeur.png\" alt=\"coeur\" title=\"Coup de coeur du bénévole\"/>";
	      }
      }
      else
      {
              if (isset($row->note))
		      $output .= Games::get_medal_from_note($row->note, 'small');
      }
      $output .= "</td>";
      if ($compact == 1)
      {
	$output .= "<td><a href='".$game->get_link()."'>".htmlspecialchars($game->get_name())."</a></td></tr>";
      }
      else
      {
	      $output .="<td><a href='".$game->get_link()."'>".htmlspecialchars($game->get_name())."</a></td><td>".$game->get_players()."</td><td>".$game->get_length()."</td><td>".$game->get_age()."</td><td>".Games::get_location($game->get_location())."</tr>";
	}
    }
    $output .= "</table>";
    return $output;
  }

  function list_new_games($list_id, $compact=0)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT COUNT(gl.game_id) as note, g.id,g.name, g.minplayers, g.maxplayers, g.length, g.age, g.location FROM `".$wpdb->prefix."games_by_list` as l, `".$wpdb->prefix."games`as g LEFT OUTER JOIN `".$wpdb->prefix."games_by_list` as gl ON g.id=gl.game_id AND (gl.type='usercoeur' OR gl.type='userbest') WHERE l.type='list' AND g.id=l.game_id AND l.list_id=%s and g.new='1' GROUP BY g.id ORDER BY g.name ASC", $list_id);
    $res = $wpdb->get_results($query);
    return Games::display_list($res, $list_id, $compact);
  }

  function list_games($list_id, $compact=0, $type='list')
  {
    global $wpdb;
    if (gettype($list_id) == 'array')
    {
	    $query = "SELECT COUNT(DISTINCT gl.game_id, gl.list_id, gl.type) as note, g.id,g.name, g.minplayers, g.maxplayers, g.length, g.age, g.location FROM `".$wpdb->prefix."games_by_list` as l, `".$wpdb->prefix."games`as g LEFT OUTER JOIN `".$wpdb->prefix."games_by_list` as gl ON g.id=gl.game_id AND (gl.type='usercoeur' OR gl.type='userbest') WHERE g.id=l.game_id AND (";
	    $first=1;
	    foreach ($list_id as $id)
	    {
		    if ($first)
		    {
			$first = 0;
		    }
		    else
		    {
			    $query .= " OR ";

		    }
	    	$query .= "l.list_id='".esc_sql($id)."'";
	    }
	    $query .= ") AND l.type='".esc_sql($type)."' GROUP BY g.id ORDER BY g.name ASC";
    }
    else
    {
	    if ($type=='user') // si c'est de type user on n'a pas la colonne note mais la colonne coeur a la place
	    {
                    $query = $wpdb->prepare("SELECT IF(gl.game_id,1,0) as coeur, g.id,g.name, g.minplayers, g.maxplayers, g.length, g.age, g.location FROM `".$wpdb->prefix."games_by_list` as l, `".$wpdb->prefix."games`as g LEFT OUTER JOIN `".$wpdb->prefix."games_by_list` as gl ON gl.list_id=%s AND gl.game_id=g.id AND gl.type='usercoeur' WHERE g.id=l.game_id AND l.list_id=%s AND l.type=%s GROUP BY g.id ORDER BY g.name ASC", $list_id, $list_id, $type);
	    }
	    else
	    {
		    $query = $wpdb->prepare("SELECT COUNT(gl.game_id) as note, g.id,g.name, g.minplayers, g.maxplayers, g.length, g.age, g.location FROM `".$wpdb->prefix."games_by_list` as l, `".$wpdb->prefix."games`as g LEFT OUTER JOIN `".$wpdb->prefix."games_by_list` as gl ON gl.game_id=g.id AND (gl.type='usercoeur' OR gl.type='userbest') WHERE g.id=l.game_id AND l.list_id=%s AND l.type=%s GROUP BY g.id ORDER BY g.name ASC", $list_id, $type);
	    }
    }
    $res = $wpdb->get_results($query);
    return Games::display_list($res, $list_id, $compact, $type);
  }

  function get_search_parameters(&$search)
  {
    $ret = array();
    foreach ($search as $key => &$item)
    {
      if (!is_array($item) && !is_numeric($item) && $key != 'emplacement' && substr($key,0,3) != 'op_') // les seuls champs non numerique est l'emplacement et les op_*
      {
        unset($search[$key]);
        continue;
      }
      elseif (is_array($item))
      {
        foreach ($item as $key2=>$item2)
        {
          if (!is_numeric($item2) && $key != 'emplacement' && substr($key,0,3)!='op_')
          {
            unset($item[$key2]);
          }
        }
        if (count($item) == 0)
        {
          unset ($search[$key]);
          continue;
        }
        elseif (count($item) == 1)
        {
          $search[$key] = $item[0];
        }
      }
      switch ($key)
      {
      case 'categories':
        if (is_array($item))
        {
          $tmp_array=array();
          foreach($item as $cat)
          {
            array_push($tmp_array,Games::get_category_name($cat));
          }
	  array_push($ret, "Jeux appartenant aux catégories ".Games::display_array($tmp_array, $search['op_categories']).".");
        }
        else
        {
          array_push($ret, "Jeux appartenant à la catégorie ".Games::get_category_name($item));
        }
        break;
      case 'mechanics':
        if (is_array($item))
        {
          $tmp_array=array();
          foreach($item as $mecha)
          {
            array_push($tmp_array, Games::get_mechanic_name($mecha));
          }
          array_push($ret, "Jeux avec les mécanismes ".Games::display_array($tmp_array, $search['op_mechanics']).".");
        }
        else
        {
          array_push($ret, "Jeux avec le mécanisme ".Games::get_mechanic_name($item));
        }
        break;
      case 'designers':
	if (!is_array($item))
	{
	  $item=array($item);
	}
        $tmp_array=array();
        foreach($item as $designer)
        {
          array_push($tmp_array, Games::get_person_name($designer));
        }
        array_push($ret, "Jeux créés par ".Games::display_array($tmp_array, $search['op_designers']).".");
        break;
      case 'artists':
	if (!is_array($item))
	{
	  $item=array($item);
	}
        $tmp_array=array();
        foreach($item as $illust)
        {
          array_push($tmp_array, Games::get_person_name($illust));
        }
        array_push($ret, "Jeux illustrés par ".Games::display_array($tmp_array, $search['op_artists']).".");
        break;
      case 'players':
	switch ($search['op_players'])
	{
	case 'sug':
          array_push($ret, "Recommandé pour $item joueurs");
	  break;
	case 'best':
	  array_push($ret, "Idéal pour $item joueurs");
	  break;
	default:
          array_push($ret, "Pour $item joueurs");
	  break;
	}
        break;
      case 'bestplayers':
        array_push($ret, "Idéal pour $item joueurs");
        break;
      case 'age':
        array_push($ret, "À partir de $item ans");
        break;
      case 'length':
        array_push($ret, "Durée maximum de $item minutes");
        break;
      case 'publisher':
        array_push($ret, "Publié par ".Games::get_publisher_name($item));
        break;
      case 'emplacement':
	$emplacements = Games::get_list_choice('emplacement', 0);
	if (!is_array($item))
	{
	  $item=array($item);
	}
        $tmp_array=array();
        foreach($item as $emplacement)
        {
	  array_push($tmp_array, $emplacements[$emplacement]);
	}
	array_push($ret, "Rangé dans la catégorie ".Games::display_array($tmp_array, "OR").".");
	break;
      case 'op_categories':
      case 'op_mechanics':
      case 'op_designers':
      case 'op_artists':
      case 'op_players':
	 break; # rien à faire, mais on ne supprime pas ces entrés de la table
      default:
        unset($search[$key]);
        break;
      }
    }
    return $ret;
  }

  function get_search_uri($search,$list_id,$type='list')
  {
    if (is_array($list_id)) 
    {
      $list_id_uri = "list_id[]=".implode("&amp;list_id[]=", array_map("urlencode",$list_id));
    }
    else
    {
      $list_id_uri = "list_id=".urlencode($list_id);
    }
    $uri = get_permalink(get_option('bgg_post_id'))."?".$list_id_uri."&amp;type=".urlencode($type)."&amp;search";
    foreach ($search as $key => &$item)
    {
      if (!is_array($item))
      {
        $uri .= "&amp;".urlencode($key)."=".urlencode($item);
      }
      else
      {
        foreach ($item as $item2)
        {
          $uri .= "&amp;".urlencode($key)."[]=".urlencode($item2);
        }
      }
    }
    return $uri;
  }

  function list_games_by_search($search,$list_id,$type='list')
  {
    global $wpdb;
    $subquery="";
    $where="";
    foreach ($search as $key => $tmpitem)
    {
      $tmp_where="";
      switch ($key)
      {
      case 'categories':
      case 'mechanics':
      case 'designers':
      case 'artists':
        $op = isset($search['op_'.$key])?$search['op_'.$key]:'OR';
        if (!is_array($tmpitem))
        {
          $tmpitem=array($tmpitem);
        }
        $temp_subquery='';
        if ($op == 'AND')
        {
          foreach ($tmpitem as $item)
          {
            $temp_subquery2="SELECT game_id FROM `".$wpdb->prefix."games_".$key."` WHERE id='".$item."'";
            if ($temp_subquery != "")
            {
              $temp_subquery = $temp_subquery2." AND game_id IN (".$temp_subquery.")";
            }
            else
            {
              $temp_subquery = $temp_subquery2;
            }
          }
        }
        else
        {
          foreach ($tmpitem as $item)
          {
            if ($temp_subquery == "")
            {
              $temp_subquery = "SELECT game_id FROM `".$wpdb->prefix."games_".$key."` WHERE id='".$item."'";
            }
            else
            {
              $temp_subquery.= " OR id='".$item."'";
            }
          }
        }
        if ($subquery != "")
        {
          $subquery = $temp_subquery." AND game_id IN (".$subquery.")";
        }
        else
        {
          $subquery = $temp_subquery;
        }
        break;
      case 'players':
        if (!is_numeric($tmpitem))
          continue;
	switch ($search['op_players'])
	{
	case 'sug':
	  $tmp_where = "FIND_IN_SET('".$tmpitem."', sugplayers)";
	  break;
	case 'best':
	  $tmp_where = "FIND_IN_SET('".$tmpitem."', bestplayers)";
	  break;
	default:
          $tmp_where = "maxplayers >= ".$tmpitem." AND minplayers <= ".$tmpitem;
	  break;
	}
        break;
      case 'age':
        if (!is_numeric($tmpitem))
          continue;
        $tmp_where = "age <= ".$tmpitem;
        break;
      case 'length':
        if (!is_numeric($tmpitem))
          continue;
        $tmp_where = "length <= ".$tmpitem;
        break;
      case 'publisher':
        if (!is_numeric($tmpitem))
          continue;
        $tmp_where = "publisher = ".$tmpitem;
        break;
      case 'emplacement':
        $emplacements = Games::get_list_choice('emplacement', 0);
        foreach ($tmpitem as $item)
        {
          if (!array_key_exists($item, $emplacements))
            continue;
          if ($tmp_where == "")
          {
            $tmp_where = "(location = '".$item."'";
          }
          else
          {
            $tmp_where .= " OR location = '".$item."'";
          }
        }
        $tmp_where .= ")";
        break;
      }
      if ($tmp_where == "")
        continue;
      if ($where == "")
      {
        $where = $tmp_where;
      } 
      else
      {
        $where .= " AND ".$tmp_where;
      }
    }
    $query = "SELECT COUNT(gl.game_id) as note, g.id, g.name, g.minplayers, g.maxplayers, g.length, g.age, g.location FROM `".$wpdb->prefix."games_by_list` as l, `".$wpdb->prefix."games`as g LEFT OUTER JOIN `".$wpdb->prefix."games_by_list` as gl ON g.id=gl.game_id AND (gl.type='usercoeur' OR gl.type='userbest') WHERE l.type='".esc_sql($type)."' AND g.id=l.game_id AND (";
    if (gettype($list_id) == 'array')
    {
      $tmp_query = '';
      foreach ($list_id as $id)
      {
        if ($tmp_query != '')
        {
          $tmp_query .= " OR ";
        }
        $tmp_query .= "l.list_id='".esc_sql($id)."'";
      }
      $query .= $tmp_query;
    }
    else
    {
      $query .= "l.list_id='".esc_sql($list_id)."'";
    }
    $query .= ")";

    if ($where != "")
    {
      $query .= " AND ".$where;
    }
    if ($subquery != "")
    {
      $query .= " AND g.id IN (".$subquery.")";
    }
    $query .= " GROUP BY g.id ORDER BY g.name";
    $res = $wpdb->get_results($query);
    return Games::display_list($res,$list_id);
  }

  function get_list_choice($type, $list_id, $type_list='list')
  {
    global $wpdb;
    if ($type == "emplacement")
    {
      return array('xs'=>'XS','s' => 'S', 'm' => 'M', 'l' => 'L', 'ludo' => 'Ludopathèque', 'party' => 'Ambiance', '2' => 'Jeux&nbsp;à&nbsp;deux', 'famille' => 'Famille', 'adresse' => 'Adresse', 'lettres' => 'Lettres', 'coop' => 'Jeux&nbsp;coopératifs', 'vitrine' => 'Vitrine', 'sac' => 'Sac', 'wargame' => 'Wargame');
    }
    if ($type == 'artists' || $type == 'designers')
    {
      $type_name = 'people_name';
    }
    else
    {
      $type_name = $type.'_name';
    }
    $tmp_query = '';
    if (gettype($list_id) == 'array')
    {
      foreach ($list_id as $id)
      {
        if ($tmp_query != '')
        {
          $tmp_query .= " OR ";
        }
        $tmp_query .= "l.list_id='".esc_sql($id)."'";
      }
    }
    else
    {
      $tmp_query .= "l.list_id='".esc_sql($list_id)."'";
    }
    $query = $wpdb->prepare("SELECT a.id as id, b.name as name FROM `".$wpdb->prefix."games_$type` as a, `".$wpdb->prefix."games_$type_name` as b WHERE a.id=b.id AND a.game_id IN (SELECT id FROM `".$wpdb->prefix."games` as g, `".$wpdb->prefix."games_by_list` as l WHERE l.type=%s AND g.id=l.game_id AND (".$tmp_query.")) ORDER BY b.name ASC", $type_list);
    $res = $wpdb->get_results($query);
    $ret = array();
    foreach ($res as $row)
    {
      $ret[$row->id] = $row->name;
    }
    return $ret;
  }

  function get_person_name($pers)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT name FROM `".$wpdb->prefix."games_people_name` WHERE id=%d", $pers);
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      return $row->name;
    }
    return 'Inconnu';
  }

  function get_publisher_name($pub)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT name FROM `".$wpdb->prefix."games_publishers_name` WHERE id=%d", $pub);
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      return $row->name;
    }
    return 'Inconnu';
  }

  function get_category_name($cat)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT name FROM `".$wpdb->prefix."games_categories_name` WHERE id=%d", $cat);
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      return $row->name;
    }
    return 'Inconnu';
  }

  function get_mechanic_name($mec)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT name FROM `".$wpdb->prefix."games_mechanics_name` WHERE id=%d", $mec);
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      return $row->name;
    }
    return 'Inconnu';
  }

  function get_list_id($list)
  {
echo "$row";
    global $wpdb;
    return $wpdb->get_var($wpdb->prepare("SELECT id FROM `".$wpdb->prefix."games_lists` WHERE name=%s", $list));
  }

  static function display_sublist_form($list_id, $return_url, $type='list')
  {
    global $wpdb;
    $output = "";
    if ($type == 'list')
    {
    	$query = $wpdb->prepare("SELECT g.name, gl1.game_id, gl2.list_id, l.last_collid FROM `".$wpdb->prefix."games_by_list` as gl1 INNER JOIN `".$wpdb->prefix."games_lists` as l ON gl1.list_id = l.parent AND l.id=$list_id AND gl1.type='list' INNER JOIN `".$wpdb->prefix."games` as g on g.id=gl1.game_id LEFT JOIN `".$wpdb->prefix."games_by_list` as gl2 ON gl1.game_id=gl2.game_id AND gl2.list_id=$list_id AND gl2.type='list' ORDER BY g.name", $list_id, $list_id);
    }
    elseif ($type == 'user')
    {
      # on verifie que la ligne du user existe
      $query = $wpdb->prepare("SELECT last_collid, parent FROM `".$wpdb->prefix."games_userlists` WHERE id=%s LIMIT 1", $list_id);
      $row = $wpdb->get_row($query);
      $parent = get_option('bgg_default_list');
      $last_collid = 0;
      if (!$row)
      {
        echo $query = $wpdb->prepare("INSERT INTO `".$wpdb->prefix."games_userlists` VALUES (%s, %d, '0')", $list_id, get_option('bgg_default_list'));
	$wpdb->query($query);
      }
      else
      {
        $last_collid = $row->last_collid;
        $parent = $row->parent;

      }
      $query = $wpdb->prepare("SELECT g.name, g.bggid, gl1.game_id, gl2.list_id as list_id, gl3.list_id as list_id_coeur, gl4.list_id as list_id_best
	        FROM `".$wpdb->prefix."games_by_list` as gl1 
		INNER JOIN `".$wpdb->prefix."games` as g on g.id=gl1.game_id 
		LEFT JOIN `".$wpdb->prefix."games_by_list` as gl2 ON gl1.game_id=gl2.game_id AND gl2.list_id=%s AND gl2.type='user' 
		LEFT JOIN `".$wpdb->prefix."games_by_list` as gl3 ON gl1.game_id=gl3.game_id AND gl3.list_id=%s AND gl3.type='usercoeur' 
		LEFT JOIN `".$wpdb->prefix."games_by_list` as gl4 ON gl1.game_id=gl4.game_id AND gl4.list_id=%s AND gl4.type='userbest' 
		WHERE gl1.list_id=%s 
		ORDER BY g.name", $list_id, $list_id, $list_id, $parent);
      		$output .= "<p>#1 correspond à votre jeu préféré<br/>&hearts; correspond à vos coups de coeur<br/>? contient les jeux que vous savez expliquer</p>";
    }
    else
    {
      return "Type $type inconnu.";
    }
    $output .= '<form name="edit" action="'.$return_url.'" method="post"><input type="hidden" name="list_id" value="'.$list_id.'"/><input type="hidden" name="action" value="edit2"/>';
    $output .= 'Afficher <input type="button" value="Nouveaux" onclick="jQuery(\'.explold\').hide()"/><input type="button" value="Tous" onclick="jQuery(\'.explold\').show();"/><br/><br/><input type="submit" value="Valider"/> <br/><br/>';
    $output .= 'Sélectionnez les jeux qui appartiennent à la liste<br/>';
    $output .= '<table>'."\n";
    if ($type == 'user')
    {
      $output .= '<tr><td>#1</td><td>&hearts;</td><td>?</td><td></td></tr>';
    }
    $res = $wpdb->get_results($query);
    foreach ($res as $row)
    {
      $output .= '<tr';
      if ($row->game_id > $last_collid)
      {
	      $output .= ' class="explnew"';
      }
      else
      {
	      $output .= ' class="explold"';
      }
      $output .= '>';
      if ($type == 'user')
      {
        $output .= '<td><input type="radio" name="best" value="'.$row->game_id.'"';
        if ($row->list_id_best == $list_id)
          $output .= ' checked="checked"';
        $output .= '/></td><td><input type="checkbox" name="coeur[]" value="'.$row->game_id.'"';
        if ($row->list_id_coeur == $list_id)
          $output .= ' checked="checked"';
	$output .= '/></td>';
      }
      $output .= '<td><input type="checkbox" name="games[]" value="'.$row->game_id.'"';
      if ($row->list_id == $list_id)
          $output .= ' checked="checked"';
      $output .= '/></td><td';
      $output .= '><a href="http://boardgamegeek.com/boardgame/'.$row->bggid.'">'.$row->name.'</a></td></tr>';
      $output .= "\n";
    }
    $output .= '</table>';
    $output .= '<input type="submit" value="Valider"/>';
    $output .= '</form>';
    return $output;
  }

  function submit_sublist_form($list_id, $data, $edit_url, $type='list')
  {
    global $wpdb;
    $output = '';
    if ($type == 'list')
    {
      $query = $wpdb->prepare("SELECT parent FROM `".$wpdb->prefix."games_lists` WHERE id=%s", $list_id);
    }
    else
    {
      $query = $wpdb->prepare("SELECT parent FROM `".$wpdb->prefix."games_userlists` WHERE id=%s", $list_id);
    }
    $parent = $wpdb->get_var($query);
    if (!$parent)
    {
      $output .= "Liste $list_id invalide";
    }
    else
    {
      if ($type == 'list')
      {
        $query = $wpdb->prepare("UPDATE `".$wpdb->prefix."games_lists` SET last_collid = (SELECT game_id FROM `".$wpdb->prefix."games_by_list` WHERE type='list' AND list_id=%s ORDER BY game_id DESC LIMIT 1) where id=%s", $parent, $list_id);
      }
      elseif ($type == 'user' || $type == 'usercoeur' || $type == 'userbest')
      {
	# maj date dernière modif
        $query = $wpdb->prepare("REPLACE INTO `".$wpdb->prefix."games_users` VALUES (%s, NOW())", $list_id);
        $wpdb->query($query);
	# maj coll id
        $query = $wpdb->prepare("UPDATE `".$wpdb->prefix."games_userlists` SET last_collid = (SELECT game_id FROM `".$wpdb->prefix."games_by_list` WHERE type='list' AND list_id=%s ORDER BY game_id DESC LIMIT 1) where id=%s", $parent, $list_id);
      }
      else
      {
        return "Type $type inconnu.\n";
      }
      $wpdb->query($query);
      $query = $wpdb->prepare("DELETE FROM `".$wpdb->prefix."games_by_list` WHERE type=%s AND list_id=%s", $type, $list_id);
      $wpdb->query($query);
      $i=0;
  
      $values = "";
      foreach($data as $key=>$value)
      {
        if ($values != "")
          $values .= ", ";
        $values .= "('".esc_sql($value)."', '".esc_sql($list_id)."', '$type')";
        $i++;
      }

      $query = "INSERT INTO `".$wpdb->prefix."games_by_list` VALUES $values";
      $wpdb->query($query);
      $output .= "<p>La liste a été mise à jour et contient désormais $i éléments</p><p><a href=\"$edit_url\">Retourner à l'édition</a></p><p><a href=\"".get_permalink(get_option('bgg_post_id')).'?list_id='.$list_id."&amp;type=".$type."\">Voir la liste</a></p>";
      if ($type == 'user' && function_exists('trombi_get_url'))
      {
        $output.="<p><a href=\"".trombi_get_url().'?id='.$list_id."\">Voir sa fiche</a></p>";
      }
    }
    return $output;
  }

  function get_medal($game_id,$size)
  {
	  global $wpdb;
	  $query = $wpdb->prepare("SELECT count(*) FROM `".$wpdb->prefix."games_by_list` WHERE game_id=%d AND (type='usercoeur' OR type='userbest')", $game_id);
	  return Games::get_medal_from_note($wpdb->get_var($query), $size);
  }

  function get_medal_from_note($note, $size='small')
  {
      if ($note>=get_option('bgg_bronze'))
      {
        $img = "bronze";
	if ($note>=get_option('bgg_silver') && $note<get_option('bgg_gold'))
		$img = 'silver';
	elseif ($note>=get_option('bgg_gold'))
		$img = 'gold';
	if ($size == 'small')
	{
		$img .= '_small';
	}
        return "<img src=\"".get_template_directory_uri()."/images/".$img.".png\" alt=\"medaille\" title=\"Appréciation des bénévoles : ".$note."\"/>";
      }
  }

  static function get_last_user_update($user_id)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT date FROM `".$wpdb->prefix."games_users` as gu WHERE id=%s", $user_id);
    return $wpdb->get_var($query);
  }
    

  static function get_last_coeur($limit=30)
  {
    global $wpdb;
    $query = $wpdb->prepare("SELECT gu.id as username, UNIX_TIMESTAMP(date) as date, gl1.game_id as bestid, g.name as bestname FROM `".$wpdb->prefix."games_users` as gu LEFT JOIN `".$wpdb->prefix."games_by_list` as gl1 ON gu.id=gl1.list_id AND gl1.type='userbest' JOIN `wp_games` as g on g.id=gl1.game_id ORDER BY date DESC LIMIT %d", $limit);
    $res = $wpdb->get_results($query);
    $output = array();
    foreach ($res as $row)
    {
	$user_info = get_user_by('login', $row->username);
        $output[$row->date] = array("name" => trim($user_info->display_name), "username" =>  $row->username, "best" => $row->bestname, "bestid" => $row->bestid);
    }
    return $output;
  }

  static function get_last_coeur_date($timezone = 'server')
  {
    global $wpdb;
    $query = "SELECT date FROM `".$wpdb->prefix."games_users` as gu LEFT JOIN `".$wpdb->prefix."games_by_list` as gl1 ON gu.id=gl1.list_id AND gl1.type='userbest' ORDER BY date DESC LIMIT 1";
    return $wpdb->get_var($query);
  }

  // fonction pour afficher les jeux sur la page de recherche
  static function search_game($keyword)
  {
    global $wpdb;
    $list_id = get_option('bgg_default_list');
    $query = "SELECT * FROM `".$wpdb->prefix."games` as g, `".$wpdb->prefix."games_by_list` as gl WHERE gl.game_id=g.id AND gl.list_id='".$list_id."' AND (name LIKE '%$keyword%' OR comment LIKE '%$keyword%' OR altnames LIKE '%$keyword%') ORDER by NAME";
    $res = $wpdb->get_results($query);
    if (!$res) return False;
    return Games::display_list($res, $list_id, 0);
  }
}

?>
